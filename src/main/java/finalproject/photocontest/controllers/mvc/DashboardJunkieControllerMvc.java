package finalproject.photocontest.controllers.mvc;

import finalproject.photocontest.models.ContestCategory;
import finalproject.photocontest.models.Filter;
import finalproject.photocontest.models.dtos.dtosMvc.JunkieContestFilterDto;
import finalproject.photocontest.services.contracts.CategoryService;
import finalproject.photocontest.services.contracts.ContestService;
import finalproject.photocontest.services.contracts.LoginServiceMvc;
import finalproject.photocontest.services.contracts.PhotoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import java.util.List;

import static finalproject.photocontest.models.enums.FilterOption.*;

@Controller
@RequestMapping("/dashboard")
public class DashboardJunkieControllerMvc {

    private final LoginServiceMvc loginService;
    private final ContestService contestService;
    private final CategoryService categoryService;
    private final PhotoService photoService;

    @Autowired
    public DashboardJunkieControllerMvc(LoginServiceMvc loginService, ContestService contestService,
                                        CategoryService categoryService, PhotoService photoService) {
        this.loginService = loginService;
        this.contestService = contestService;
        this.categoryService = categoryService;
        this.photoService = photoService;
    }

    @ModelAttribute("categories")
    public List<ContestCategory> fetchAllCategories() {
        return categoryService.getAll();
    }

    @GetMapping("/open")
    public String showAllOpenContests(Model model, HttpSession session) {

        try {
            var user = loginService.tryGetUser(session);
            var filter = new Filter().add(USER_ID, String.valueOf(user.getUserId()));

            model.addAttribute("user", user);
            model.addAttribute("form", new JunkieContestFilterDto());
            model.addAttribute("openContests", contestService.getAll());
            // temporarily showing all contests for better testing of views
        } catch (RuntimeException e) {
            model.addAttribute("error", e.getMessage());
            return "error";
        }

        return "dashboard-junkie-open-contests";
    }

    @PostMapping("/open")
    public String showFilteredOpenContests(Model model, HttpSession session,
                                           @Valid @ModelAttribute("form") JunkieContestFilterDto form,
                                           BindingResult binding) {

        var user = loginService.tryGetUser(session);

        if (binding.hasErrors()) {
            model.addAttribute("user", user);
            model.addAttribute("form", new JunkieContestFilterDto());
            model.addAttribute("openContests", contestService.getAll());

            return "dashboard-junkie-open-contests";
        }

        try {

            var filter = new Filter().add(USER_ID, String.valueOf(user.getUserId()));
            if (form.getCategoryId() != null) filter.add(CATEGORY_ID, String.valueOf(form.getCategoryId()));
            if (form.getSearch() != null) filter.add(SEARCH, form.getSearch());

            model.addAttribute("user", user);
            model.addAttribute("openContests", contestService.getContestsAvailableToUser(filter));

        } catch (RuntimeException e) {
            model.addAttribute("error", e.getMessage());
            return "error";
        }

        return "dashboard-junkie-open-contests";
    }

    @GetMapping("/my-photos")
    public String showAllUserPhotos(Model model, HttpSession session) {

        try {
            var user = loginService.tryGetUser(session);
            var filter = new Filter().add(AUTHOR_ID, String.valueOf(user.getUserId()));

            model.addAttribute("user", user);
            model.addAttribute("form", new JunkieContestFilterDto());
            model.addAttribute("photos", photoService.getAll(filter));
        } catch (RuntimeException e) {
            model.addAttribute("error", e.getMessage());
            return "error";
        }
        return "dashboard-junkie-my-photos";
    }

    @PostMapping("/my-photos")
    public String showFilteredUserPhotos(Model model, HttpSession session,
                                         @Valid @ModelAttribute("form") JunkieContestFilterDto form,
                                         BindingResult binding) {

        if (binding.hasErrors()) return "dashboard-junkie-my-photos";

        try {
            var user = loginService.tryGetUser(session);

            var filter = new Filter().add(USER_ID, String.valueOf(user.getUserId()));
            if (form.getCategoryId() != null) filter.add(CATEGORY_ID, String.valueOf(form.getCategoryId()));
            if (form.getSearch() != null) filter.add(SEARCH, form.getSearch());

            model.addAttribute("user", user);
            model.addAttribute("photos", photoService.getAll(filter));

        } catch (RuntimeException e) {
            model.addAttribute("error", e.getMessage());
            return "error";
        }

        return "dashboard-junkie-my-photos";
    }

    @GetMapping("/my-contests")
    public String showUserContestsParticipating(Model model, HttpSession session) {

        try {
            var user = loginService.tryGetUser(session);
            var filter = new Filter().add(USER_ID, String.valueOf(user.getUserId()));

            model.addAttribute("user", user);
            model.addAttribute("form", new JunkieContestFilterDto());
            model.addAttribute("userContests", contestService.getAll(filter));
        } catch (RuntimeException e) {
            model.addAttribute("error", e.getMessage());
            return "error";
        }
        return "dashboard-junkie-my-contests";
    }

    @PostMapping("/my-contests")
    public String showUserContestsParticipating(Model model, HttpSession session,
                                                @Valid @ModelAttribute("form") JunkieContestFilterDto form,
                                                BindingResult binding) {

        if (binding.hasErrors()) return "dashboard-junkie-my-contests";

        try {
            var user = loginService.tryGetUser(session);

            var filter = new Filter().add(USER_ID, String.valueOf(user.getUserId()));
            if (form.getCategoryId() != null) filter.add(CATEGORY_ID, String.valueOf(form.getCategoryId()));
            if (form.getSearch() != null) filter.add(SEARCH, form.getSearch());

            model.addAttribute("user", user);
            model.addAttribute("userContests", contestService.getAll(filter));

        } catch (RuntimeException e) {
            model.addAttribute("error", e.getMessage());
            return "error";
        }

        return "dashboard-junkie-my-contests";
    }


}