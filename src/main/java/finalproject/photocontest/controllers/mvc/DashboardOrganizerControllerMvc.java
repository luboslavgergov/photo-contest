package finalproject.photocontest.controllers.mvc;

import finalproject.photocontest.models.ContestCategory;
import finalproject.photocontest.models.Filter;
import finalproject.photocontest.models.dtos.dtosMvc.JunkieContestFilterDto;
import finalproject.photocontest.models.dtos.dtosMvc.OrganizerContestFilterDto;
import finalproject.photocontest.models.dtos.dtosMvc.UserFilterDto;
import finalproject.photocontest.models.enums.ContestPhase;
import finalproject.photocontest.models.enums.UserRank;
import finalproject.photocontest.services.contracts.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import java.util.List;

import static finalproject.photocontest.constants.GlobalConstants.ROLE_ORGANIZER;
import static finalproject.photocontest.models.enums.FilterOption.*;

@Controller
@RequestMapping("/dashboard")
public class DashboardOrganizerControllerMvc {

    private final LoginServiceMvc loginService;
    private final ContestService contestService;
    private final CategoryService categoryService;
    private final PhotoService photoService;
    private final UserService userService;

    @Autowired
    public DashboardOrganizerControllerMvc(LoginServiceMvc loginService, ContestService contestService,
                                           CategoryService categoryService, PhotoService photoService,
                                           UserService userService) {
        this.loginService = loginService;
        this.contestService = contestService;
        this.categoryService = categoryService;
        this.photoService = photoService;
        this.userService = userService;
    }

    @ModelAttribute("categories")
    public List<ContestCategory> fetchAllCategories() {
        return categoryService.getAll();
    }

    @ModelAttribute("phases")
    public ContestPhase[] fetchAllPhases() {
        return ContestPhase.values();
    }

    @ModelAttribute("userRanks")
    public UserRank[] fetchAllUserRanks() {
        return UserRank.values();
    }

    @GetMapping("/contests")
    public String showAllContests(Model model, HttpSession session) {
        try {
            var user = loginService.verifyAuthorization(session, ROLE_ORGANIZER);

            model.addAttribute("user", user);
            model.addAttribute("form", new OrganizerContestFilterDto());
            model.addAttribute("allContests", contestService.getAll());
        } catch (RuntimeException e) {
            model.addAttribute("error", e.getMessage());
            return "error";
        }
        return "dashboard-organizer-contests";
    }

    @PostMapping("/contests")
    public String showFilteredContests(Model model, HttpSession session,
                                       @Valid @ModelAttribute("form") OrganizerContestFilterDto form,
                                       BindingResult binding) {

        if (binding.hasErrors()) return "dashboard-organizer-contests";

        try {
            var user = loginService.verifyAuthorization(session, ROLE_ORGANIZER);

            var filter = new Filter();
            if (form.getCategoryId() != null) filter.add(CATEGORY_ID, String.valueOf(form.getCategoryId()));
            if (form.getPhase() != null && !form.getPhase().isBlank())
                filter.add(PHASE, form.getPhase());
            if (form.getSearch() != null) filter.add(SEARCH, form.getSearch());
            if (form.getOpen() != null && form.getOpen()) filter.add(STATUS, "open");
            if (form.getOpen() != null && !form.getOpen()) filter.add(STATUS, "invitational");

            model.addAttribute("user", user);
            model.addAttribute("allContests", contestService.getAll(filter));

        } catch (RuntimeException e) {
            model.addAttribute("error", e.getMessage());
            return "error";
        }

        return "dashboard-organizer-contests";
    }

    @PostMapping("/contests/{contestId}/delete")
    public String deleteContest(Model model, HttpSession session, @PathVariable int contestId,
                                @Valid @ModelAttribute("form") OrganizerContestFilterDto form,
                                BindingResult binding) {

        if (binding.hasErrors()) return "dashboard-organizer-contests";

        try {
            var user = loginService.verifyAuthorization(session, ROLE_ORGANIZER);

            var filter = new Filter();
            if (form.getCategoryId() != null) filter.add(CATEGORY_ID, String.valueOf(form.getCategoryId()));
            if (form.getPhase() != null && !form.getPhase().isBlank())
                filter.add(PHASE, form.getPhase());
            if (form.getSearch() != null) filter.add(SEARCH, form.getSearch());
            if (form.getOpen() != null && form.getOpen()) filter.add(STATUS, "open");
            if (form.getOpen() != null && !form.getOpen()) filter.add(STATUS, "invitational");

            contestService.delete(contestId);

            model.addAttribute("user", user);
            model.addAttribute("allContests", contestService.getAll(filter));

        } catch (RuntimeException e) {
            model.addAttribute("error", e.getMessage());
            return "error";
        }

        return "redirect:/dashboard/contests";
    }

    @GetMapping("/users")
    public String showAllUsers(Model model, HttpSession session) {
        try {
            var user = loginService.verifyAuthorization(session, ROLE_ORGANIZER);

            model.addAttribute("user", user);
            model.addAttribute("form", new UserFilterDto());
            model.addAttribute("allUsers", userService.getAll());
            return "dashboard-organizer-users";
        } catch (RuntimeException e) {
            model.addAttribute("error", e.getMessage());
            return "error";
        }
    }

    @PostMapping("/users")
    public String showFilteredUsers(Model model, HttpSession session,
                                    @Valid @ModelAttribute("form") UserFilterDto form, BindingResult binding) {

        if (binding.hasErrors()) return "dashboard-organizer-users";

        try {
            var user = loginService.verifyAuthorization(session, ROLE_ORGANIZER);

            var filter = new Filter();
            if (form.getMinPoints() != null) filter.add(MIN_RANKING_POINTS, String.valueOf(form.getMinPoints()));
            if (form.getRole() != null && !form.getRole().isBlank()) filter.add(ROLE, form.getRole());
            if (form.getSearch() != null) filter.add(SEARCH, form.getSearch());

            model.addAttribute("user", user);
            model.addAttribute("allUsers", userService.getAll(filter));

        } catch (RuntimeException e) {
            model.addAttribute("error", e.getMessage());
            return "error";
        }

        return "dashboard-organizer-users";
    }

    @PostMapping("/users/{userId}/suspend")
    public String suspendUser(Model model, HttpSession session, @PathVariable int userId) {

        try {
            var user = loginService.verifyAuthorization(session, ROLE_ORGANIZER);

            userService.suspend(user, userId);
            model.addAttribute("user", user);
            model.addAttribute("form", new UserFilterDto());
            model.addAttribute("allUsers", userService.getAll());
        } catch (RuntimeException e) {
            model.addAttribute("error", e.getMessage());
            return "error";
        }
        return "redirect:/dashboard/users";
    }

    @PostMapping("/users/{userId}/restore")
    public String restoreUser(Model model, HttpSession session, @PathVariable int userId) {

        try {
            var user = loginService.verifyAuthorization(session, ROLE_ORGANIZER);

            userService.restore(user, userId);

            model.addAttribute("user", user);
            model.addAttribute("form", new UserFilterDto());
            model.addAttribute("allUsers", userService.getAll());
        } catch (RuntimeException e) {
            model.addAttribute("error", e.getMessage());
            return "error";
        }

        return "redirect:/dashboard/users";
    }

    @GetMapping("/photos")
    public String showAllPhotos(Model model, HttpSession session) {

        try {
            var user = loginService.verifyAuthorization(session, ROLE_ORGANIZER);

            model.addAttribute("user", user);
            model.addAttribute("form", new JunkieContestFilterDto());
            model.addAttribute("photos", photoService.getAll());
        } catch (RuntimeException e) {
            model.addAttribute("error", e.getMessage());
            return "error";
        }

        return "dashboard-organizer-photos";
    }

    @PostMapping("/photos")
    public String showFilteredPhotos(Model model, HttpSession session,
                                     @Valid @ModelAttribute("form") JunkieContestFilterDto form,
                                     BindingResult binding) {

        if (binding.hasErrors()) return "dashboard-organizer-photos";

        try {
            var user = loginService.verifyAuthorization(session, ROLE_ORGANIZER);

            var filter = new Filter();
            if (form.getCategoryId() != null) filter.add(CATEGORY_ID, String.valueOf(form.getCategoryId()));
            if (form.getSearch() != null) filter.add(SEARCH, form.getSearch());

            model.addAttribute("user", user);
            model.addAttribute("photos", photoService.getAll(filter));

        } catch (RuntimeException e) {
            model.addAttribute("error", e.getMessage());
            return "error";
        }

        return "dashboard-organizer-photos";
    }

    @GetMapping("/contests/{contestId}/invite")
    public String showUsersToInvite(@PathVariable int contestId, Model model, HttpSession session) {

        try {


            var user = loginService.verifyAuthorization(session, ROLE_ORGANIZER);
            var contest = contestService.getById(contestId);

            model.addAttribute("contest", contest);
            model.addAttribute("user", user);
            model.addAttribute("form", new UserFilterDto());
            model.addAttribute("allUsers",
                    userService.getAllActiveJunkiesNotParticipatingAlready(contestId));

        } catch (RuntimeException e) {
            model.addAttribute("error", e.getMessage());
            return "error";
        }
        return "dashboard-organizer-invite-users";
    }

    @PostMapping("/contests/{contestId}/invite")
    public String showFilteredUsersInvite(@PathVariable int contestId, Model model, HttpSession session,
                                          @ModelAttribute("form") UserFilterDto form, BindingResult binding) {

        if (binding.hasErrors()) return "dashboard-organizer-invite-users";

        try {
            var user = loginService.verifyAuthorization(session, ROLE_ORGANIZER);
            var contest = contestService.getById(contestId);

            var filter = new Filter();
            if (form.getMinPoints() != null) filter.add(MIN_RANKING_POINTS, String.valueOf(form.getMinPoints()));
            if (form.getRole() != null && !form.getRole().isBlank()) filter.add(ROLE, form.getRole());
            if (form.getSearch() != null) filter.add(SEARCH, form.getSearch());

            model.addAttribute("contest", contest);
            model.addAttribute("user", user);
            model.addAttribute("allUsers", userService.getAll(filter));

        } catch (RuntimeException e) {
            model.addAttribute("error", e.getMessage());
            return "error";
        }

        return "dashboard-organizer-invite-users";
    }


    @PostMapping("/contests/{contestId}/invite/{userId}")
    public String inviteUser(@PathVariable int contestId, @PathVariable int userId, Model model, HttpSession session,
                             @ModelAttribute("form") UserFilterDto form, BindingResult binding) {

        try {
            var user = loginService.verifyAuthorization(session, ROLE_ORGANIZER);
            var contest = contestService.getById(contestId);
            contestService.inviteUserToContest(contestId, userId);

            model.addAttribute("contest", contest);
            model.addAttribute("user", user);
            model.addAttribute("form", new UserFilterDto());
            model.addAttribute("allUsers",
                    userService.getAllActiveJunkiesNotParticipatingAlready(contestId));

            return "redirect:/dashboard/contests/" + contest.getContestId() + "/invite";
        } catch (RuntimeException e) {
            model.addAttribute("error", e.getMessage());
            return "error";
        }
    }
}