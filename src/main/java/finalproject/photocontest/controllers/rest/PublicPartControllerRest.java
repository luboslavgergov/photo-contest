package finalproject.photocontest.controllers.rest;

import finalproject.photocontest.models.ContestCategory;
import finalproject.photocontest.models.User;
import finalproject.photocontest.models.daos.PhotoEvaluationDao;
import finalproject.photocontest.models.daos.UserRankingInfoDao;
import finalproject.photocontest.models.dtos.dtosRest.CreateUserDto;
import finalproject.photocontest.services.contracts.*;
import finalproject.photocontest.utils.ExceptionHandler;
import finalproject.photocontest.utils.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/v1")
public class PublicPartControllerRest {

    private final ModelMapper mapper;
    private final UserService userService;
    private final ContestService contestService;
    private final EvaluationService evaluationService;
    private final CategoryService categoryService;
    private final PhotoService photoService;

    @Autowired
    public PublicPartControllerRest(ModelMapper mapper, UserService userService, ContestService contestService,
                                    EvaluationService evaluationService, CategoryService categoryService,
                                    PhotoService photoService) {
        this.mapper = mapper;
        this.userService = userService;
        this.contestService = contestService;
        this.evaluationService = evaluationService;
        this.categoryService = categoryService;
        this.photoService = photoService;
    }

    @GetMapping("/counts/users")
    public int serveTotalUsersCount() {
        return userService.getAll().size();
    }

    @GetMapping("/counts/contests")
    public int serveTotalContestsCount() {
        return contestService.getAll().size();
    }

    @GetMapping("/counts/photos")
    public int serveTotalPhotosCount() {
        return photoService.getAll().size();
    }

    @GetMapping("/categories")
    public List<ContestCategory> listAllCategories() {
        return categoryService.getAll();
    }

    @GetMapping("/top/photos")
    public List<PhotoEvaluationDao> listTopRankedPhotos() {
        return evaluationService.getTopPhotos(10);
    }

    @GetMapping("/top/users")
    public List<UserRankingInfoDao> listTopRankedUsers() {
        return evaluationService.getTopUsers(10);
    }

    @PostMapping("/register")
    public User handleUserRegistration(@RequestBody @Valid CreateUserDto dto) {

        try {
            var user = mapper.fromDto(dto);
            return userService.create(user);

        } catch (RuntimeException e) {
            throw ExceptionHandler.process(e);
        }
    }
}
