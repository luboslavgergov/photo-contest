package finalproject.photocontest.controllers.rest;

import finalproject.photocontest.models.Contest;
import finalproject.photocontest.models.Filter;
import finalproject.photocontest.services.LoginServiceImpl;
import finalproject.photocontest.services.contracts.ContestService;
import finalproject.photocontest.utils.ExceptionHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

import static finalproject.photocontest.constants.GlobalConstants.*;
import static finalproject.photocontest.models.enums.FilterOption.*;

@RestController
@RequestMapping(value = "/api/v1/dashboard", headers = {PASSWORD, USERNAME})
public class DashboardJurorControllerRest {

    private final LoginServiceImpl loginService;
    private final ContestService contestService;

    @Autowired
    public DashboardJurorControllerRest(ContestService contestService, LoginServiceImpl loginService) {
        this.contestService = contestService;
        this.loginService = loginService;
    }

    @GetMapping("/jury-contests")
    public List<Contest> listUserContestAsJuror(@RequestHeader HttpHeaders headers,
                                                @RequestParam Optional<String> phase,
                                                @RequestParam Optional<Integer> categoryId,
                                                @RequestParam Optional<String> search) {
        try {
            var authenticatedUser = loginService.authenticateUser(headers);
            loginService.authorizeRole(authenticatedUser, ROLE_JUNKIE, ROLE_ORGANIZER);

            var filter = new Filter()
                    .add(JUROR_ID, String.valueOf(authenticatedUser.getUserId()));
            phase.ifPresent(ph -> filter.add(PHASE, ph));
            categoryId.ifPresent(cat -> filter.add(CATEGORY_ID, String.valueOf(cat)));
            search.ifPresent(s -> filter.add(SEARCH, s));

            return contestService.getContestsAsJuror(filter);

        } catch (RuntimeException e) {
            throw ExceptionHandler.process(e);
        }
    }
}

