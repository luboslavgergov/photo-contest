package finalproject.photocontest.validation;

import finalproject.photocontest.exceptions.*;
import finalproject.photocontest.models.PhotoEvaluation;
import finalproject.photocontest.models.User;
import finalproject.photocontest.models.dtos.dtosRest.CreatePhotoDto;
import finalproject.photocontest.models.enums.ContestPhase;
import finalproject.photocontest.repos.contracts.PhotoObjectRepository;
import finalproject.photocontest.repos.contracts.PhotoReviewRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.Set;

import static finalproject.photocontest.constants.validations.PhotoConstants.*;

@Service
public class PhotoValidationServiceImpl implements finalproject.photocontest.validation.contracts.PhotoValidationService {

    private static final Set<String> ALLOWED_EXTENSIONS = Set.of(".jpg", ".png", ".jpeg");
    private final PhotoObjectRepository photoObjectRepository;
    private final PhotoReviewRepository photoReviewRepository;

    @Autowired
    public PhotoValidationServiceImpl(PhotoObjectRepository photoObjectRepository, PhotoReviewRepository photoReviewRepository) {
        this.photoObjectRepository = photoObjectRepository;
        this.photoReviewRepository = photoReviewRepository;
    }

    @Override
    public PhotoValidationServiceImpl ensureUnique(PhotoEvaluation photoEvaluation) {

        try {
            photoReviewRepository.getPhotoReviewByJurorAndPhotoId(
                    photoEvaluation.getReview().getJuror().getUserId(), photoEvaluation.getPhoto().getPhotoId());
            throw new IllegalRequestException(PHOTO_ALREADY_EVALUATED_EX_MESSAGE);

        } catch (EntityNotFoundException e) {
            return this;
        }
    }

    @Override
    public PhotoValidationServiceImpl ensureFileNotEmpty(MultipartFile file) {

        if (file.isEmpty())
            throw new PhotoStorageException(PHOTO_CANNOT_STORE_EMPTY_FILES_EX_MESSAGE);

        return this;
    }

    @Override
    public PhotoValidationServiceImpl ensureFileContentType(MultipartFile file) {

        if (file.getContentType() == null || !file.getContentType().startsWith("image/"))
            throw new PhotoStorageException(PHOTO_INVALID_CONTENT_TYPE_EX_MESSAGE);

        return this;
    }

    @Override
    public PhotoValidationServiceImpl ensureFileExtension(MultipartFile file) {

        var extension = file.getContentType().replace("image/", ".");

        if (!ALLOWED_EXTENSIONS.contains(extension))
            throw new UnsupportedPhotoFileExtensionException(PHOTO_FILE_EXTENSION_NOT_ALLOWED_EX_MESSAGE);

        return this;
    }

    @Override
    public PhotoValidationServiceImpl ensurePermissionsToViewPhoto(int contestId, int photoId, User issuer) {

        var photo = photoObjectRepository.getById(photoId);
        var contest = photo.getContest();

        if (photo.getAuthor().getUserId() == issuer.getUserId())
            return this; // viewing own photo
        if (contest.getParticipants().contains(issuer) && contest.getPhase() == ContestPhase.FINISHED)
            return this; // viewing contest participating in photo when contest finished
        if (contest.getJurors().contains(issuer) && contest.getPhase().ordinal() > ContestPhase.PHASE_I.ordinal())
            return this; // viewing contest jurying in photo when contest past enrollment phase

        throw new AuthorizationException(issuer.getCredentials().getUsername());
    }

    @Override
    public PhotoValidationServiceImpl ensurePhotoDataConsistent(String title, String story) {
        new CreatePhotoDto(title, story);
        return this;
    }
}
