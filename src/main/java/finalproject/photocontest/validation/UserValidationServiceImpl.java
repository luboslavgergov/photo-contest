package finalproject.photocontest.validation;

import finalproject.photocontest.exceptions.EntityNotFoundException;
import finalproject.photocontest.exceptions.IllegalRequestException;
import finalproject.photocontest.models.Contest;
import finalproject.photocontest.models.User;
import finalproject.photocontest.repos.contracts.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static finalproject.photocontest.constants.validations.UserConstants.*;

@Service
public class UserValidationServiceImpl implements finalproject.photocontest.validation.contracts.UserValidationService {

    private final UserRepository userRepository;

    @Autowired
    public UserValidationServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserValidationServiceImpl ensureUsernameUnique(User user) {

        try {
            var persistedUser = userRepository.getByUsername(user.getCredentials().getUsername());
            if (persistedUser.getUserId() != user.getUserId())
                throw new IllegalRequestException(USER_USERNAME_ALREADY_TAKEN_EX_MESSAGE);

        } catch (EntityNotFoundException e) {
            // empty
        }
        return this;
    }

    @Override
    public UserValidationServiceImpl ensureIsJunkie(User user) {

        if (!user.isJunkie())
            throw new IllegalRequestException(USER_IS_NOT_JUNKIE_EX_MESSAGE);

        return this;
    }

    @Override
    public UserValidationServiceImpl ensurePermittedToAlterUserActiveStatus(User user) {

        if (!user.isOrganizer())
            throw new IllegalRequestException(USER_IS_NOT_ORGANIZER_EX_MESSAGE);

        return this;
    }

    @Override
    public UserValidationServiceImpl ensureIsEligibleForJuror(User user) {

        if (!user.isEligibleForJuror())
            throw new IllegalRequestException(USER_NOT_ELIGIBLE_FOR_JUROR_DUTY_EX_MESSAGE);

        return this;
    }

    @Override
    public UserValidationServiceImpl ensureIsNotJuror(User user, Contest contest) {

        if (contest.getJurors().contains(user))
            throw new IllegalRequestException(USER_IS_JUROR_FOR_CONTEST_CANNOT_PARTICIPATE_EX_MESSAGE);

        return this;
    }
}
