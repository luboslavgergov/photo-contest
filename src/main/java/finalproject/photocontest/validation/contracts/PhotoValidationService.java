package finalproject.photocontest.validation.contracts;

import finalproject.photocontest.models.PhotoEvaluation;
import finalproject.photocontest.models.User;
import org.springframework.web.multipart.MultipartFile;

public interface PhotoValidationService {

    PhotoValidationService ensureUnique(PhotoEvaluation photoEvaluation);

    PhotoValidationService ensureFileNotEmpty(MultipartFile file);

    PhotoValidationService ensureFileContentType(MultipartFile file);

    PhotoValidationService ensureFileExtension(MultipartFile file);

    PhotoValidationService ensurePermissionsToViewPhoto(int contestId, int photoId, User issuer);

    PhotoValidationService ensurePhotoDataConsistent(String title, String story);
}
