package finalproject.photocontest.validation;

import finalproject.photocontest.constants.GlobalConstants;
import finalproject.photocontest.exceptions.AuthorizationException;
import finalproject.photocontest.exceptions.EntityNotFoundException;
import finalproject.photocontest.exceptions.IllegalRequestException;
import finalproject.photocontest.models.Contest;
import finalproject.photocontest.models.User;
import finalproject.photocontest.models.dtos.PhotoReviewDto;
import finalproject.photocontest.models.enums.ContestPhase;
import finalproject.photocontest.models.enums.UserRank;
import finalproject.photocontest.repos.contracts.ContestRepository;
import finalproject.photocontest.repos.contracts.PhotoObjectRepository;
import finalproject.photocontest.repos.contracts.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Duration;

import static finalproject.photocontest.constants.validations.ContestConstants.*;

@Service
public class ContestValidationServiceImpl implements finalproject.photocontest.validation.contracts.ContestValidationService {

    private final ContestRepository contestRepository;
    private final UserRepository userRepository;
    private final PhotoObjectRepository photoObjectRepository;

    @Autowired
    public ContestValidationServiceImpl(ContestRepository contestRepository, UserRepository userRepository,
                                        PhotoObjectRepository photoObjectRepository) {

        this.contestRepository = contestRepository;
        this.userRepository = userRepository;
        this.photoObjectRepository = photoObjectRepository;
    }

    @Override
    public ContestValidationServiceImpl ensureUserEnrolled(Contest contest, User user) {

        if (!contest.getParticipants().contains(user))
            throw new IllegalRequestException(CONTEST_USER_NOT_ENROLLED_EX_MESSAGE);

        return this;
    }

    @Override
    public ContestValidationServiceImpl ensurePastEnrollmentPhase(Contest contest) {

        if (contest.getPhase().ordinal() <= ContestPhase.PHASE_I.ordinal())
            throw new IllegalRequestException(CONTEST_NOT_PAST_ENROLLMENT_EX_MESSAGE);

        return this;
    }

    @Override
    public ContestValidationServiceImpl ensureInEnrollmentPhase(Contest contest) {

        if (contest.getPhase().ordinal() != ContestPhase.PHASE_I.ordinal())
            throw new IllegalRequestException(CONTEST_PAST_ENROLLMENT_PHASE_EX_MESSAGE);

        return this;
    }

    @Override
    public ContestValidationServiceImpl ensureFinished(Contest contest) {

        if (contest.getPhase() != ContestPhase.FINISHED)
            throw new IllegalRequestException(CONTEST_NOT_FINISHED_EX_MESSAGE);

        return this;
    }

    @Override
    public ContestValidationServiceImpl ensureNotFinished(Contest contest) {

        if (contest.getPhase() == ContestPhase.FINISHED)
            throw new IllegalRequestException(CONTEST_FINISHED_EX_MESSAGE);

        return this;
    }

    @Override
    public ContestValidationServiceImpl ensureOpen(Contest contest) {

        if (!contest.isOpen())
            throw new IllegalRequestException(CONTEST_INVITATIONAL_ONLY_EX_MESSAGE);

        return this;
    }

    @Override
    public ContestValidationServiceImpl ensureTitleUnique(Contest contest) {

        try {
            contestRepository.getByTitle(contest.getTitle());
            throw new IllegalRequestException(CONTEST_TITLE_ALREADY_EXISTS_EX_MESSAGE);
        } catch (EntityNotFoundException e) {
            return this;
        }
    }

    @Override
    public ContestValidationServiceImpl ensureScheduleTimelineConsistent(Contest contest) {

        if (!(contest.getStartPhase1().isBefore(contest.getStartPhase2())
                && contest.getStartPhase2().isBefore(contest.getEndPhase2())))
            throw new IllegalRequestException(CONTEST_PHASES_NOT_SCHEDULED_CORRECT_EX_MESSAGE);

        return this;
    }

    @Override
    public ContestValidationServiceImpl ensurePhaseDurationsConsistent(Contest contest) {

        var phaseOneDuration = Duration.between(contest.getStartPhase1(), contest.getStartPhase2());
        var phaseTwoDuration = Duration.between(contest.getStartPhase2(), contest.getEndPhase2());

        if (phaseOneDuration.toDays() < 1 || phaseTwoDuration.toDays() >= 31)
            throw new IllegalRequestException(CONTEST_DURATION_OF_PHASE_I_EX_MESSAGE);

        if (phaseTwoDuration.toHours() < 1 || phaseTwoDuration.toHours() >= 24)
            throw new IllegalRequestException(CONTEST_DURATION_OF_PHASE_II_EX_MESSAGE);

        return this;
    }

    @Override
    public ContestValidationServiceImpl ensureJurorsEligible(Contest contest) {

        if (!contest.getJurors().stream().allMatch(juror ->
                juror.getUserRole().getName().equalsIgnoreCase(GlobalConstants.ROLE_ORGANIZER)
                        || juror.getRank().ordinal() >= UserRank.MASTER.ordinal()))
            throw new IllegalRequestException(CONTEST_USER_NOT_ELIGIBLE_AS_JUROR_EX_MESSAGE);

        return this;
    }

    @Override
    public ContestValidationServiceImpl ensureNotAlreadyEnrolled(Contest contest, User user) {

        if (contest.getParticipants().contains(user))
            throw new IllegalRequestException(CONTEST_USER_ALREADY_ENROLLED_EX_MESSAGE);

        return this;
    }

    @Override
    public ContestValidationServiceImpl ensureEnrolled(Contest contest, User user) {

        if (!contest.getParticipants().contains(user))
            throw new IllegalRequestException(CONTEST_USER_NOT_ENROLLED_EX_MESSAGE);

        return this;
    }

    @Override
    public ContestValidationServiceImpl ensureNotVisibleToUsers(Contest contest) {
        if (contest.getPhase() != ContestPhase.NOT_STARTED)
            throw new IllegalRequestException(CONTEST_CANNOT_DELETE_ALREADY_VISIBLE_EX_MESSAGE);

        return this;
    }

    @Override
    public ContestValidationServiceImpl ensurePermissionsToViewContestPhotos(int userId, int contestId) {

        var contest = contestRepository.getById(contestId);
        var user = userRepository.getById(userId);

        if (!(contest.getJurors().contains(user) && contest.getPhase().ordinal() >= ContestPhase.PHASE_I.ordinal())
                && !(contest.getParticipants().contains(user) && contest.getPhase() == ContestPhase.FINISHED))
            throw new AuthorizationException(user.getCredentials().getUsername());

        return this;
    }

    @Override
    public ContestValidationServiceImpl ensurePhotoPresent(int contestId, int photoId) {

        var contest = contestRepository.getById(contestId);
        var photo = photoObjectRepository.getById(photoId);

        if (photo.getContest().getContestId() != contestId)
            throw new IllegalRequestException(CONTEST_PHOTO_DOES_NOT_EXIST_EX_MESSAGE);

        return this;
    }

    @Override
    public ContestValidationServiceImpl ensureUserHasNoPhotoUploaded(User user, Contest contest) {

        if (contest.getPhotos().stream()
                .anyMatch(ph -> ph.getAuthor().getUserId() == user.getUserId()))
            throw new IllegalRequestException(CONTEST_USER_ALREADY_UPLOADED_PHOTO_EX_MESSAGE);

        return this;
    }

    @Override
    public ContestValidationServiceImpl ensureUserUploadedPhoto(User user, Contest contest) {

        if (contest.getPhotos().stream()
                .noneMatch(ph -> ph.getAuthor().getUserId() == user.getUserId()))
            throw new IllegalRequestException(CONTEST_USER_HAS_NOT_UPLOADED_PHOTO_EX_MESSAGE);

        return this;
    }

    @Override
    public ContestValidationServiceImpl ensurePermissionsToViewContest(int contestId, int userId) {

        var user = userRepository.getById(userId);
        var contest = contestRepository.getById(contestId);

        if (!user.isOrganizer()
                && !contest.getParticipants().contains(user)
                && !contest.getJurors().contains(user))
            throw new AuthorizationException(user.getCredentials().getUsername());

        return this;
    }

    @Override
    public ContestValidationServiceImpl ensurePhotoReviewConsistent(PhotoReviewDto dto) {
        var score = dto.getScore();
        var comment = dto.getComment();
        var isValidPhoto = dto.isValid();

        if (score == 0 && isValidPhoto)
            throw new IllegalRequestException(CONTEST_PHOTOS_WHICH_ARE_NOT_RELEVANT_EX_MESSAGE);

        if (comment.isBlank() && isValidPhoto)
            throw new IllegalRequestException(CONTEST_EMPTY_COMMENTS_EX_MESSAGE);

        return this;
    }
}