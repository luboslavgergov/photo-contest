package finalproject.photocontest.models.daos;

import finalproject.photocontest.models.Photo;
import finalproject.photocontest.models.Review;

import java.util.Set;

public class PhotoEvaluationDao {

    private Photo photo;

    private Set<Review> reviews;

    private double photoScoreAvg;

    private int rankingPlace;

    public PhotoEvaluationDao() {
    }

    public PhotoEvaluationDao(Photo photo, Set<Review> reviews, double photoScoreAvg, int rankingPlace) {
        this.photo = photo;
        this.reviews = reviews;
        this.photoScoreAvg = photoScoreAvg;
        this.rankingPlace = rankingPlace;
    }

    public Photo getPhoto() {
        return photo;
    }

    public void setPhoto(Photo photo) {
        this.photo = photo;
    }

    public Set<Review> getReviews() {
        return reviews;
    }

    public void setReviews(Set<Review> reviews) {
        this.reviews = reviews;
    }

    public double getPhotoScoreAvg() {
        return photoScoreAvg;
    }

    public void setPhotoScoreAvg(double photoScoreAvg) {
        this.photoScoreAvg = photoScoreAvg;
    }

    public int getRankingPlace() {
        return rankingPlace;
    }

    public void setRankingPlace(int rankingPlace) {
        this.rankingPlace = rankingPlace;
    }
}
