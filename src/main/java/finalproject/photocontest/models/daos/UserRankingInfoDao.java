package finalproject.photocontest.models.daos;

import finalproject.photocontest.models.User;

public class UserRankingInfoDao {

    private User user;
    private String currentRank;
    private int currentPoints;
    private String forNextRank;
    private int firstPlacesAwarded;
    private int secondPlacesAwarded;
    private int thirdPlacesAwarded;

    public UserRankingInfoDao() {
    }

    public UserRankingInfoDao(User user, String currentRank, String forNextRank, int currentPoints, int firstPlaces,
                              int secondPlaces, int thirdPlaces) {
        this.user = user;
        this.currentRank = currentRank;
        this.forNextRank = forNextRank;
        this.currentPoints = currentPoints;
        this.firstPlacesAwarded = firstPlaces;
        this.secondPlacesAwarded = secondPlaces;
        this.thirdPlacesAwarded = thirdPlaces;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getForNextRank() {
        return forNextRank;
    }

    public void setForNextRank(String forNextRank) {
        this.forNextRank = forNextRank;
    }

    public String getCurrentRank() {
        return currentRank;
    }

    public void setCurrentRank(String currentRank) {
        this.currentRank = currentRank;
    }

    public int getCurrentPoints() {
        return currentPoints;
    }

    public void setCurrentPoints(int currentPoints) {
        this.currentPoints = currentPoints;
    }

    public int getFirstPlacesAwarded() {
        return firstPlacesAwarded;
    }

    public void setFirstPlacesAwarded(int firstPlacesAwarded) {
        this.firstPlacesAwarded = firstPlacesAwarded;
    }

    public int getSecondPlacesAwarded() {
        return secondPlacesAwarded;
    }

    public void setSecondPlacesAwarded(int secondPlacesAwarded) {
        this.secondPlacesAwarded = secondPlacesAwarded;
    }

    public int getThirdPlacesAwarded() {
        return thirdPlacesAwarded;
    }

    public void setThirdPlacesAwarded(int thirdPlacesAwarded) {
        this.thirdPlacesAwarded = thirdPlacesAwarded;
    }
}
