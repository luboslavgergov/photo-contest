package finalproject.photocontest.models.dtos.dtosRest;

import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Future;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.time.LocalDateTime;

import static finalproject.photocontest.constants.models.ModelsConstants.*;

public class CreateContestDto {

    @NotNull
    @Length(min = 5, max = 50, message = CONTEST_TITLE_VALIDATION_MESSAGE)
    private String title;

    @Future(message = CONTEST_START_PHASE_I_VALIDATION_MESSAGE)
    private LocalDateTime startPhase1;

    @Future(message = CONTEST_START_PHASE_II_VALIDATION_MESSAGE)
    private LocalDateTime startPhase2;

    @Future(message = CONTEST_END_VALIDATION_MESSAGE)
    private LocalDateTime endPhase2;

    @Positive(message = CATEGORY_ID_VALIDATION_MESSAGE)
    private int categoryId;

    private boolean isOpen;

    private int[] addedJurors;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public LocalDateTime getStartPhase1() {
        return startPhase1;
    }

    public void setStartPhase1(LocalDateTime startPhase1) {
        this.startPhase1 = startPhase1;
    }

    public LocalDateTime getStartPhase2() {
        return startPhase2;
    }

    public void setStartPhase2(LocalDateTime startPhase2) {
        this.startPhase2 = startPhase2;
    }

    public LocalDateTime getEndPhase2() {
        return endPhase2;
    }

    public void setEndPhase2(LocalDateTime endPhase2) {
        this.endPhase2 = endPhase2;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public boolean isOpen() {
        return isOpen;
    }

    public void setOpen(boolean open) {
        isOpen = open;
    }

    public int[] getAddedJurors() {
        return addedJurors;
    }

    public void setAddedJurors(int[] addedJurors) {
        this.addedJurors = addedJurors;
    }
}
