package finalproject.photocontest.models.dtos.dtosRest;

import org.hibernate.validator.constraints.Length;
import javax.validation.constraints.NotNull;

import static finalproject.photocontest.constants.models.ModelsConstants.*;

public class CreatePhotoDto {

    @NotNull
    @Length(min = 5, max = 50, message = PHOTO_TITLE_VALIDATION_MESSAGE)
    private String title;

    @NotNull(message = PHOTO_STORY_VALIDATION_MESSAGE)
    private String story;

    public CreatePhotoDto() {
    }

    public CreatePhotoDto(String title, String story) {
        this.title = title;
        this.story = story;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getStory() {
        return story;
    }

    public void setStory(String story) {
        this.story = story;
    }
}
