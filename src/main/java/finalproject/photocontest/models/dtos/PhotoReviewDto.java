package finalproject.photocontest.models.dtos;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotNull;

import static finalproject.photocontest.constants.models.ModelsConstants.*;

public class PhotoReviewDto {

    @Range(min = 1, max = 10, message = PHOTO_REVIEW_SCORE_VALIDATION_MESSAGE)
    private int score;

    @NotNull(message = PHOTO_REVIEW_COMMENT_VALIDATION_MESSAGE)
    @Length(min = 5, max = 500, message = PHOTO_REVIEW_COMMENT_LENGTH_VALIDATION_MESSAGE)
    private String comment;

    private boolean isValid = true;

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public boolean isValid() {
        return isValid;
    }

    public void setValid(boolean valid) {
        isValid = valid;
    }
}
