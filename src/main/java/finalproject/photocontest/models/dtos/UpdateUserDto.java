package finalproject.photocontest.models.dtos;

import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

import static finalproject.photocontest.constants.models.ModelsConstants.*;

public class UpdateUserDto {

    private int id;

    @NotNull
    @Length(min = 2, max = 20, message = FIRST_NAME_VALIDATION_MESSAGE)
    private String firstName;

    @NotNull
    @Length(min = 2, max = 20, message = LAST_NAME_VALIDATION_MESSAGE)
    private String lastName;

    @NotNull
    @Length(min = 8, max = 20, message = PASSWORD_VALIDATION_MESSAGE)
    private String password;

    @NotNull
    @Length(min = 8, max = 20, message = PASSWORD_VALIDATION_MESSAGE)
    private String confirmPassword;

    public UpdateUserDto() {
    }

    public UpdateUserDto(String password, String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.password = password;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
