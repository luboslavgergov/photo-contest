package finalproject.photocontest.models.dtos.dtosMvc;

import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

import static finalproject.photocontest.constants.models.ModelsConstants.*;

public class RegisterDto {

    @NotNull
    @Length(min = 2, max = 20, message = USERNAME_VALIDATION_MESSAGE)
    private String username;

    @NotNull
    @Length(min = 8, max = 20, message = PASSWORD_VALIDATION_MESSAGE)
    private String password;

    @NotNull
    @Length(min = 8, max = 20, message = PASSWORD_VALIDATION_MESSAGE)
    private String confirmPassword;

    @NotNull
    @Length(min = 2, max = 20, message = FIRST_NAME_VALIDATION_MESSAGE)
    private String firstName;

    @NotNull
    @Length(min = 2, max = 20, message = LAST_NAME_VALIDATION_MESSAGE)
    private String lastName;

    public RegisterDto() {
    }


    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }
    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }
}
