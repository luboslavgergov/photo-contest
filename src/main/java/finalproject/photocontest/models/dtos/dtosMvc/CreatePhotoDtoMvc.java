package finalproject.photocontest.models.dtos.dtosMvc;

import org.hibernate.validator.constraints.Length;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotNull;

import static finalproject.photocontest.constants.models.ModelsConstants.PHOTO_STORY_VALIDATION_MESSAGE;
import static finalproject.photocontest.constants.models.ModelsConstants.PHOTO_TITLE_VALIDATION_MESSAGE;

public class CreatePhotoDtoMvc {


    @NotNull
    @Length(min = 5, max = 50, message = PHOTO_TITLE_VALIDATION_MESSAGE)
    private String title;

    @NotNull(message = PHOTO_STORY_VALIDATION_MESSAGE)
    private String story;

    @NotNull
    private MultipartFile file;

    public CreatePhotoDtoMvc() {
    }

    public CreatePhotoDtoMvc(String title, String story) {
        this.title = title;
        this.story = story;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getStory() {
        return story;
    }

    public void setStory(String story) {
        this.story = story;
    }

    public MultipartFile getFile() {
        return file;
    }

    public void setFile(MultipartFile file) {
        this.file = file;
    }
}
