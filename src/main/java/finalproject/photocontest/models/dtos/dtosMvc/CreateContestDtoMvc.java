package finalproject.photocontest.models.dtos.dtosMvc;

import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

import static finalproject.photocontest.constants.models.ModelsConstants.*;

public class CreateContestDtoMvc {

    @NotNull
    @Length(min = 5, max = 50, message = CONTEST_TITLE_VALIDATION_MESSAGE)
    private String title;

    private String startPhase1;

    private String startPhase2;

    private String endPhase2;

    @Positive(message = CATEGORY_ID_VALIDATION_MESSAGE)
    private int categoryId;

    private boolean isOpen;

    private Integer[] addedJurors;

    private Integer[] invitedJunkies;

    private String coverPhotoLink;

    public CreateContestDtoMvc() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getStartPhase1() {
        return startPhase1;
    }

    public void setStartPhase1(String startPhase1) {
        this.startPhase1 = startPhase1;
    }

    public String getStartPhase2() {
        return startPhase2;
    }

    public void setStartPhase2(String startPhase2) {
        this.startPhase2 = startPhase2;
    }

    public String getEndPhase2() {
        return endPhase2;
    }

    public void setEndPhase2(String endPhase2) {
        this.endPhase2 = endPhase2;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public boolean isOpen() {
        return isOpen;
    }

    public void setOpen(boolean open) {
        isOpen = open;
    }

    public Integer[] getAddedJurors() {
        return addedJurors;
    }

    public void setAddedJurors(Integer[] addedJurors) {
        this.addedJurors = addedJurors;
    }

    public Integer[] getInvitedJunkies() {
        return invitedJunkies;
    }

    public void setInvitedJunkies(Integer[] invitedJunkies) {
        this.invitedJunkies = invitedJunkies;
    }

    public String getCoverPhotoLink() {
        return coverPhotoLink;
    }

    public void setCoverPhotoLink(String coverPhotoLink) {
        this.coverPhotoLink = coverPhotoLink;
    }
}