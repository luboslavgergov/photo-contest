package finalproject.photocontest.models.enums;

import static finalproject.photocontest.constants.GlobalConstants.*;

public enum UserRank {

    JUNKIE(JUNKIE_RANK_STARTING_POINTS, "Junkie"),
    ENTHUSIAST(ENTHUSIAST_RANK_STARTING_POINTS, "Enthusiast"),
    MASTER(MASTER_RANK_STARTING_POINTS, "Master"),
    WISE_AND_BENEVOLENT_PHOTO_DICTATOR(WBPD_RANK_STARTING_POINTS, "WBPhD"),
    UNTOUCHABLE(-1, "Don't mess with this guy!");

    int minPoints;
    String rankAsString;

    UserRank(int rankStartingPoints, String rankAsString) {
        this.minPoints = rankStartingPoints;
        this.rankAsString = rankAsString;
    }

    public static UserRank getRankFor(int rankingPoints) {
        if (rankingPoints < JUNKIE_RANK_STARTING_POINTS) return UserRank.UNTOUCHABLE;
        if (rankingPoints < ENTHUSIAST_RANK_STARTING_POINTS) return UserRank.JUNKIE;
        if (rankingPoints < MASTER_RANK_STARTING_POINTS) return UserRank.ENTHUSIAST;
        if (rankingPoints < WBPD_RANK_STARTING_POINTS) return UserRank.MASTER;
        return UserRank.WISE_AND_BENEVOLENT_PHOTO_DICTATOR;
    }

    public int getRankMinPoints() {
        return this.minPoints;
    }

    public String getPointsTillNextRank(int currentPoints) {
        if (this == UNTOUCHABLE) return "Untouchable";
        if (this == WISE_AND_BENEVOLENT_PHOTO_DICTATOR) return "Already at max rank";
        return String.valueOf(values()[this.ordinal() + 1].minPoints - currentPoints);
    }

    @Override
    public String toString() {
        return this.rankAsString;
    }
}
