package finalproject.photocontest.models;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "reviews")
public class Review {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "review_id")
    private int reviewId;

    @Column(name = "score")
    private int score;

    @Column(name = "comment")
    private String comment;

    @OneToOne
    @JoinColumn(name = "juror_id")
    private User juror;

    public Review() {
    }

    public Review(int score, String comment, User juror) {
        this.score = score;
        this.comment = comment;
        this.juror = juror;
    }

    public int getReviewId() {
        return reviewId;
    }

    public void setReviewId(int reviewId) {
        this.reviewId = reviewId;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int scores) {
        this.score = scores;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public User getJuror() {
        return juror;
    }

    public void setJuror(User juror) {
        this.juror = juror;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Review review = (Review) o;
        return score == review.score && Objects.equals(comment, review.comment) && Objects.equals(juror, review.juror);
    }

    @Override
    public int hashCode() {
        return Objects.hash(score, comment, juror);
    }
}
