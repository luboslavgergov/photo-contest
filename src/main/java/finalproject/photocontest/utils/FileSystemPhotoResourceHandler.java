package finalproject.photocontest.utils;

import finalproject.photocontest.exceptions.EntityNotFoundException;
import finalproject.photocontest.models.Photo;
import finalproject.photocontest.repos.contracts.PhotoObjectRepository;
import finalproject.photocontest.repos.contracts.PhotoResourceRepository;
import finalproject.photocontest.utils.contracts.PhotoResourceHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

@Service
public class FileSystemPhotoResourceHandler implements PhotoResourceHandler {

    private final PhotoObjectRepository photoObjectRepository;
    private final PhotoResourceRepository photoFileRepository;

    @Autowired
    public FileSystemPhotoResourceHandler(PhotoObjectRepository photoObjectRepository,
                                          PhotoResourceRepository photoFileRepository) {

        this.photoObjectRepository = photoObjectRepository;
        this.photoFileRepository = photoFileRepository;
    }

    @Transactional
    @Override
    public Photo persistMultipartPhoto(Photo photo, MultipartFile file) {
        photoObjectRepository.create(photo);
        photoFileRepository.store(file, photo.getAssociatedFileName());
        return photo;
    }

    @Transactional
    @Override
    public boolean deleteMultipartPhotoIfPresent(int userId, int contestId) {

        try {
            var userContestPhoto = photoObjectRepository.getByUserAndContestId(userId, contestId);
            photoObjectRepository.delete(userContestPhoto);
            photoFileRepository.deleteByName(userContestPhoto.getAssociatedFileName());
            return true;
        } catch (EntityNotFoundException e) {
            return false;
        }
    }
}
