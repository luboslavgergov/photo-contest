package finalproject.photocontest.utils.contracts;

import org.springframework.scheduling.annotation.Scheduled;

public interface ScheduledTasks {

   void processFinishedContests();

    @Scheduled(fixedRate = 5 * 60 * 1000)
    void kickOutUsersWithoutPhotos();
}
