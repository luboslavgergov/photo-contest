package finalproject.photocontest.utils.contracts;

import finalproject.photocontest.models.Photo;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

public interface PhotoResourceHandler {

    @Transactional
    Photo persistMultipartPhoto(Photo photo, MultipartFile file);

    @Transactional
    boolean deleteMultipartPhotoIfPresent(int userId, int contestId);
}
