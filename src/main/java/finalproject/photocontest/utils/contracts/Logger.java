package finalproject.photocontest.utils.contracts;

public interface Logger {

    void logInfo(String message);

    void logWarning(String message);

    void logWarning(String message, Throwable e);
}
