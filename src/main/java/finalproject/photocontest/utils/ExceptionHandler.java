package finalproject.photocontest.utils;

import finalproject.photocontest.utils.contracts.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import static finalproject.photocontest.constants.GlobalConstants.*;

@Component
public class ExceptionHandler {

    private static final Logger logger = new ConsoleLogger();

    private ExceptionHandler() {
    }

    public static ResponseStatusException process(RuntimeException e) {

        switch (e.getClass().getSimpleName()) {

            case ENTITY_NOT_FOUND_EXCEPTION:
                return new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());

            case DUPLICATE_ENTITY_EXCEPTION:
                return new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());

            case AUTHORIZATION_EXCEPTION:
                return new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());

            case AUTHENTICATION_EXCEPTION:
                return new ResponseStatusException(HttpStatus.FORBIDDEN, e.getMessage());

            case ILLEGAL_REQUEST_EXCEPTION:
                return new ResponseStatusException(HttpStatus.PRECONDITION_FAILED, e.getMessage());

            case PHOTO_STORAGE_EXCEPTION:
                return new ResponseStatusException(HttpStatus.NO_CONTENT, e.getMessage());

            case FILE_EXTENSION_EXCEPTION:
                return new ResponseStatusException(HttpStatus.INSUFFICIENT_STORAGE, e.getMessage());

            default:
                logger.logWarning(e.getMessage(), e);
                return new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage());
        }
    }
}