package finalproject.photocontest.services;

import finalproject.photocontest.models.Filter;
import finalproject.photocontest.models.Photo;
import finalproject.photocontest.models.User;
import finalproject.photocontest.repos.contracts.PhotoResourceRepository;
import finalproject.photocontest.repos.contracts.PhotoObjectRepository;
import finalproject.photocontest.services.contracts.PhotoService;
import finalproject.photocontest.utils.contracts.PhotoResourceHandler;
import finalproject.photocontest.validation.contracts.ContestValidationService;
import finalproject.photocontest.validation.contracts.PhotoValidationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Service
public class PhotoServiceImpl implements PhotoService {

    private final PhotoObjectRepository photoObjectRepository;
    private final PhotoResourceRepository photoFileRepository;
    private final PhotoResourceHandler photoResourceHandler;
    private final ContestValidationService contestValidationService;
    private final PhotoValidationService photoValidationService;

    @Autowired
    public PhotoServiceImpl(PhotoObjectRepository photoObjectRepository, PhotoResourceRepository photoFileRepository,
                            PhotoResourceHandler photoResourceHandler, ContestValidationService contestValidationService,
                            PhotoValidationService photoValidationService) {

        this.photoObjectRepository = photoObjectRepository;
        this.photoFileRepository = photoFileRepository;
        this.photoResourceHandler = photoResourceHandler;
        this.contestValidationService = contestValidationService;
        this.photoValidationService = photoValidationService;
    }

    @Override
    public Photo getById(int id) {
        return photoObjectRepository.getById(id);
    }

    @Override
    public Photo getByContestIdAndPhotoId(int contestId, int photoId, User issuer) {

        contestValidationService
                .ensurePhotoPresent(contestId, photoId);
        photoValidationService
                .ensurePermissionsToViewPhoto(contestId, photoId, issuer);

        return photoObjectRepository.getById(photoId);
    }

    @Override
    public List<Photo> getAll() {
        return photoObjectRepository.getAll();
    }

    @Override
    public List<Photo> getAll(Filter filter) {
        return photoObjectRepository.getAll(filter);
    }

    @Override
    public Resource loadContestPhotoFileAsResource(int contestId, int photoId, User issuer) {

        contestValidationService
                .ensurePhotoPresent(contestId, photoId);

        photoValidationService
                .ensurePermissionsToViewPhoto(contestId, photoId, issuer);

        var filename = photoObjectRepository.getById(photoId).getAssociatedFileName();
        return photoFileRepository.loadAsResource(filename);
    }

    @Override
    public Photo create(Photo photo, MultipartFile file) {

        contestValidationService
                .ensureEnrolled(photo.getContest(), photo.getAuthor())
                .ensureUserHasNoPhotoUploaded(photo.getAuthor(), photo.getContest())
                .ensureInEnrollmentPhase(photo.getContest());

        photoValidationService
                .ensureFileNotEmpty(file)
                .ensureFileContentType(file)
                .ensureFileExtension(file);

        photo.setExtension(extractFileExtension(file));

        return photoResourceHandler.persistMultipartPhoto(photo, file);
    }

    private String extractFileExtension(MultipartFile file) {
        photoValidationService.ensureFileContentType(file);
        return file.getContentType().replace("image/", ".");
    }
}
