package finalproject.photocontest.services;

import finalproject.photocontest.models.ContestCategory;
import finalproject.photocontest.repos.contracts.CategoryRepository;
import finalproject.photocontest.services.contracts.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryServiceImpl implements CategoryService {

    private final CategoryRepository categoryRepository;

    @Autowired
    public CategoryServiceImpl(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    @Override
    public ContestCategory getById(int id) {
        return categoryRepository.getById(id);
    }

    @Override
    public ContestCategory getByName(String name) {
        return categoryRepository.getByName(name);
    }

    @Override
    public List<ContestCategory> getAll() {
        return categoryRepository.getAll();
    }
}
