package finalproject.photocontest.services;

import finalproject.photocontest.exceptions.AuthenticationException;
import finalproject.photocontest.exceptions.AuthorizationException;
import finalproject.photocontest.exceptions.EntityNotFoundException;
import finalproject.photocontest.models.User;
import finalproject.photocontest.services.contracts.LoginServiceMvc;
import finalproject.photocontest.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpSession;
import java.util.Arrays;

import static finalproject.photocontest.constants.services.ServiceConstants.LOGIN_NO_LOGGED_IN_USER_EX_MESSAGE;

@Service
public class LoginServiceMvcImpl implements LoginServiceMvc {


    private final UserService userService;

    @Autowired
    public LoginServiceMvcImpl(UserService userService) {
        this.userService = userService;
    }

    public User authenticateUser(String username, String password) {
        try {
            return userService.getByCredentials(username, password);

        } catch (EntityNotFoundException e) {
            throw new AuthenticationException();
        }
    }

    public User tryGetUser(HttpSession session) {

        String currentUserUsername = (String) session.getAttribute("currentUserUsername");

        if (currentUserUsername == null) {
            throw new AuthorizationException(LOGIN_NO_LOGGED_IN_USER_EX_MESSAGE);
        }

        try {
            return userService.getByUsername(currentUserUsername);
        } catch (EntityNotFoundException e) {
            throw new AuthorizationException(LOGIN_NO_LOGGED_IN_USER_EX_MESSAGE);
        }
    }

    public User verifyAuthorization(HttpSession session, String... authorizedRoles) {
        User user = tryGetUser(session);

        if (Arrays.stream(authorizedRoles).noneMatch(role -> role.equalsIgnoreCase(user.getUserRole().getName())))
            throw new AuthorizationException(user.getCredentials().getUsername());

        return user;
    }

    public User verifyAuthorizationJury(HttpSession session, String... authorizedRoles) {
        User user = tryGetUser(session);

        if (Arrays.stream(authorizedRoles).noneMatch(role -> role.equalsIgnoreCase(user.getUserRole().getName()))
                && !user.isEligibleForJuror())
            throw new AuthorizationException(user.getCredentials().getUsername());

        return user;
    }
}