package finalproject.photocontest.services;

import finalproject.photocontest.exceptions.AuthenticationException;
import finalproject.photocontest.exceptions.AuthorizationException;
import finalproject.photocontest.exceptions.EntityNotFoundException;
import finalproject.photocontest.models.User;
import finalproject.photocontest.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;

import java.util.Arrays;

import static finalproject.photocontest.constants.GlobalConstants.PASSWORD;
import static finalproject.photocontest.constants.GlobalConstants.USERNAME;

@Service
public class LoginServiceImpl implements finalproject.photocontest.services.contracts.LoginService {

    private final UserService userService;

    @Autowired
    public LoginServiceImpl(UserService userService) {
        this.userService = userService;
    }

    public User authenticateUser(HttpHeaders headers) {

        String username = headers.getFirst(USERNAME);
        String password = headers.getFirst(PASSWORD);

        try {
            return userService.getByCredentials(username, password);

        } catch (EntityNotFoundException e) {
            throw new AuthenticationException(username);
        }
    }

    public User authorizeRole(User user, String... authorizedRoles) {
        if (Arrays.stream(authorizedRoles).noneMatch(role -> role.equalsIgnoreCase(user.getUserRole().getName())))
            throw new AuthorizationException(user.getCredentials().getUsername());
        return user;
    }
}
