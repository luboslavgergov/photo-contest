package finalproject.photocontest.services.contracts;

import finalproject.photocontest.models.Filter;
import finalproject.photocontest.models.User;

import java.util.List;

public interface UserService {

    int getPrizeCountForPlace(int userId, int placeFinished);

    User getById(int id);

    User getByUsername(String username);

    User getByCredentials(String username, String password);

    List<User> getAll();

    List<User> getAll(Filter filter);

    List<User> getAllJunkiesEligibleForJurorDuty();

    User create(User user);

    User update(User user);

    User suspend(User issuer, int userToSuspendId);

    User restore(User issuer, int userToSuspendId);

    User delete(User user);

    List<User> getAllActiveJunkies();

    List<User> getAllActiveJunkiesNotParticipatingAlready(int contestId);
}
