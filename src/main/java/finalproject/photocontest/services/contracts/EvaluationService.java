package finalproject.photocontest.services.contracts;

import finalproject.photocontest.models.Contest;
import finalproject.photocontest.models.PhotoEvaluation;
import finalproject.photocontest.models.daos.PhotoEvaluationDao;
import finalproject.photocontest.models.daos.UserRankingInfoDao;

import java.util.List;

public interface EvaluationService {

    PhotoEvaluationDao getSinglePhotoEvaluation(int userId, int contestId);

    List<PhotoEvaluationDao> getAllContestPhotoEvaluations(int userId, int contestId);

    List<PhotoEvaluationDao> getTopPhotos(int count);

    List<UserRankingInfoDao> getTopUsers(int count);

    PhotoEvaluation create(PhotoEvaluation photoEvaluation);

    void removeParticipantsWhoDidNotSubmitPhoto(Contest contest);

    void addDefaultReviewsForPhotosNotEvaluatedByJurors(Contest contest);

    void processContestWinners(Contest contest);

    void updateUserRankingPoints(int contestId);
}
