package finalproject.photocontest.services.contracts;

import finalproject.photocontest.models.User;
import org.springframework.http.HttpHeaders;

public interface LoginService {

    User authenticateUser(HttpHeaders headers);

    User authorizeRole(User user, String... authorizedRoles);

}
