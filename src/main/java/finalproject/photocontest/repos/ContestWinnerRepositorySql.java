package finalproject.photocontest.repos;

import finalproject.photocontest.models.ContestWinner;
import finalproject.photocontest.repos.contracts.ContestWinnerRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public class ContestWinnerRepositorySql implements ContestWinnerRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public ContestWinnerRepositorySql(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<ContestWinner> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<ContestWinner> query = session.createQuery(
                    "from ContestWinner order by place, pointsAvg", ContestWinner.class);

            return query.getResultList();
        }
    }

    @Override
    public List<ContestWinner> getByPlaceFinished(int contestId, int place) {
        try (Session session = sessionFactory.openSession()) {
            Query<ContestWinner> query = session.createQuery(
                    "from ContestWinner where contest.id =:contestId and place=:place order by pointsAvg desc",
                    ContestWinner.class);
            query.setParameter("contestId", contestId);
            query.setParameter("place", place);

            return query.getResultList();
        }
    }

    @Transactional
    @Override
    public ContestWinner create(ContestWinner winner) {

            try (Session session = sessionFactory.openSession()) {
                session.beginTransaction();
                session.save(winner);
                session.getTransaction().commit();
                return winner;
            }
    }

    @Override
    public List<ContestWinner> getTopRankedWinners(int count) {
        try (Session session = sessionFactory.openSession()) {
            Query<ContestWinner> query = session.createQuery(
                    "from ContestWinner order by pointsAvg desc", ContestWinner.class);

            query.setMaxResults(count);

            return query.getResultList();
        }
    }
}



