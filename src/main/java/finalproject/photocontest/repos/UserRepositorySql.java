package finalproject.photocontest.repos;

import finalproject.photocontest.exceptions.EntityNotFoundException;
import finalproject.photocontest.models.*;
import finalproject.photocontest.repos.contracts.UserRepository;
import finalproject.photocontest.utils.QueryHelper;
import finalproject.photocontest.utils.contracts.PhotoResourceHandler;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

import static finalproject.photocontest.models.enums.FilterOption.*;

@Repository
public class UserRepositorySql implements UserRepository {

    private final SessionFactory sessionFactory;
    private final PhotoResourceHandler photoResourceHandler;

    @Autowired
    public UserRepositorySql(SessionFactory sessionFactory, PhotoResourceHandler photoResourceHandler) {
        this.sessionFactory = sessionFactory;
        this.photoResourceHandler = photoResourceHandler;
    }

    @Override
    public User getById(int id) {

        try (Session session = sessionFactory.openSession()) {
            var query = session.createQuery(
                    "from User where userId=:userId and active=true", User.class);
            query.setParameter("userId", id);

            var result = query.getResultList();

            if (result.isEmpty()) throw new EntityNotFoundException("User", id);
            return result.get(0);

        }
    }

    @Override
    public User getInactiveById(int id) {

        try (Session session = sessionFactory.openSession()) {
            var query = session.createQuery(
                    "from User where userId=:userId and active=false", User.class);
            query.setParameter("userId", id);

            var result = query.getResultList();

            if (result.isEmpty()) throw new EntityNotFoundException("User", id);
            return result.get(0);
        }
    }

    @Override
    public User getByUsername(String username) {

        try (Session session = sessionFactory.openSession()) {

            var query = session.createQuery(
                    "from User where credentials.username = :username", User.class);
            query.setParameter("username", username);

            var results = query.getResultList();
            if (results.isEmpty())
                throw new EntityNotFoundException("User", "username", username);

            return results.get(0);
        }
    }

    @Override
    public User getActiveByUsername(String username) {

        try (Session session = sessionFactory.openSession()) {

            var query = session.createQuery(
                    "from User where credentials.username = :username and active=true", User.class);
            query.setParameter("username", username);

            var results = query.getResultList();
            if (results.isEmpty())
                throw new EntityNotFoundException("User", "username", username);

            return results.get(0);
        }
    }

    @Override
    public User getActiveByCredentials(String username, String password) {

        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery(
                    "from User where  credentials.username = :username " +
                            "and credentials.password = :password and active=true", User.class);

            query.setParameter("username", username);
            query.setParameter("password", password);

            var result = query.getResultList();
            if (result.isEmpty()) throw new EntityNotFoundException();

            return result.get(0);
        }
    }

    @Override
    public List<User> getTopRankedUsers(int count) {

        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery(
                    "from User u where u.active=true order by u.rankingPoints desc", User.class);

            query.setMaxResults(count);

            return query.getResultList();
        }
    }

    @Override
    public List<User> getAllActiveJunkies() {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery(
                    "from User u where u.active=true and u.userRole.name = 'junkie' order by u.rankingPoints desc", User.class);

            return query.getResultList();
        }
    }

    @Override
    public List<User> getAllJunkiesEligibleToInvite(int contestId) {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery(
                    "from User u where u.active=true and u.userRole.name = 'junkie' and " +
                            "u.userId not in (select cp.participant.userId from ContestParticipant cp " +
                            " where cp.contest.contestId = :contestId) and " +
                            " u.userId not in (select cj.juror.userId from ContestJuror cj " +
                            " where cj.contest.id = :contestId) " +
                            " order by  u.rankingPoints desc", User.class);

            query.setParameter("contestId", contestId);

            return query.getResultList();
        }
    }

    @Override
    public ContestParticipant getContestParticipant(int contestId, int userId) {

        try (Session session = sessionFactory.openSession()) {
            Query<ContestParticipant> query = session.createQuery(
                    "from ContestParticipant where participant.userId = :userId " +
                            "and contest.contestId = :contestId", ContestParticipant.class);

            query.setParameter("userId", userId);
            query.setParameter("contestId", contestId);

            var result = query.getResultList();
            if (result.isEmpty()) throw new EntityNotFoundException();

            return result.get(0);
        }
    }

    @Override
    public List<ContestParticipant> getAllContestParticipants(int contestId) {

        try (Session session = sessionFactory.openSession()) {
            Query<ContestParticipant> query = session.createQuery(
                    "from ContestParticipant where contest.contestId = :contestId", ContestParticipant.class);

            query.setParameter("contestId", contestId);

            return query.getResultList();
        }
    }

    @Override
    public int getContestAwardsCountFor(int userId, int placeFinished) {

        try (Session session = sessionFactory.openSession()) {
            var query = session.createQuery(
                    "from ContestWinner cw where cw.photo.author.userId = :userId and cw.place = :place",
                    ContestWinner.class);

            query.setParameter("userId", userId);
            query.setParameter("place", placeFinished);

            return query.getResultList().size();
        }
    }

    @Override
    public List<User> getAll() {

        try (Session session = sessionFactory.openSession()) {
            var query = session.createQuery(
                    "from User order by rankingPoints desc", User.class);

            return query.getResultList();
        }
    }

    @Override
    public List<User> getAll(Filter filter) {
        try (Session session = sessionFactory.openSession()) {
            var query = buildConditionalUserQuery(filter, session);
            return query.getResultList();
        }
    }

    @Transactional
    @Override
    public User create(User user) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(user);
            session.getTransaction().commit();

            return getById(user.getUserId());
        }
    }

    @Transactional
    @Override
    public User update(User user) {

        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(user);
            session.getTransaction().commit();

            return user;
        }
    }

    @Transactional
    @Override
    public User restore(User user) {

        try (Session session = sessionFactory.openSession()) {
            user.setActive(true);
            session.beginTransaction();
            session.update(user);
            session.getTransaction().commit();

            return user;
        }
    }

    @Transactional
    @Override
    public Contest cancelEnrolment(int contestId, int userId) {

        try (Session session = sessionFactory.openSession()) {
            var participant = getContestParticipant(contestId, userId);
            var contest = participant.getContest();

            session.beginTransaction();
            session.delete(participant);
            photoResourceHandler.deleteMultipartPhotoIfPresent(userId, contestId);

            session.getTransaction().commit();

            return contest;
        }
    }

    @Transactional
    @Override
    public User delete(User user) {

        try (Session session = sessionFactory.openSession()) {
            user.setActive(false);
            session.beginTransaction();
            session.update(user);
            session.getTransaction().commit();

            return user;
        }
    }

    private Query<User> buildConditionalUserQuery(Filter filter, Session session) {

        StringBuilder queryBuilder = new StringBuilder("from User ");
        List<String> additions = new ArrayList<>();

        if (filter.containsOption(ROLE))
            additions.add(" userRole.name =:userRole ");

        if (filter.containsOption(MIN_RANKING_POINTS))
            additions.add(" rankingPoints >= :minPoints ");

        if (filter.containsOption(SEARCH))
            additions.add(" (firstName like :search or lastName like :search or credentials.username like :search) ");

        if (!additions.isEmpty())
            queryBuilder.append(" where ").append(String.join(" and ", additions));

        queryBuilder.append("order by rankingPoints desc");

        Query<User> query = session.createQuery(queryBuilder.toString(), User.class);

        if (filter.containsOption(ROLE))
            query.setParameter("userRole", filter.get(ROLE));

        if (filter.containsOption(MIN_RANKING_POINTS))
            query.setParameter("minPoints", Integer.valueOf(filter.get(MIN_RANKING_POINTS)));

        if (filter.containsOption(SEARCH))
            query.setParameter("search", QueryHelper.getAsPartialSearchParam(filter.get(SEARCH)));

        return query;
    }
}
