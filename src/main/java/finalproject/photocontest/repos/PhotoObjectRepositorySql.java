package finalproject.photocontest.repos;

import finalproject.photocontest.exceptions.DuplicateEntityException;
import finalproject.photocontest.exceptions.EntityNotFoundException;
import finalproject.photocontest.models.*;
import finalproject.photocontest.repos.contracts.PhotoObjectRepository;
import finalproject.photocontest.utils.QueryHelper;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

import static finalproject.photocontest.models.enums.FilterOption.*;

@Repository
public class PhotoObjectRepositorySql implements PhotoObjectRepository {

    public static final String PHOTO_NOT_FOUND_EX_MESSAGE = "Photo not found";
    private final SessionFactory sessionFactory;

    @Autowired
    public PhotoObjectRepositorySql(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Photo getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            var contest = session.get(Photo.class, id);
            if (contest == null) throw new EntityNotFoundException();

            return contest;
        }
    }

    @Override
    public Photo getByUserAndContestId(int userId, int contestId) {

        try (Session session = sessionFactory.openSession()) {
            var query = session.createQuery(
                    "from Photo where author.userId = :userId and contest.contestId = :contestId", Photo.class);

            query.setParameter("userId", userId);
            query.setParameter("contestId", contestId);

            var results = query.getResultList();
            if (results.isEmpty()) throw new EntityNotFoundException(PHOTO_NOT_FOUND_EX_MESSAGE);
            if (results.size() > 1) throw new DuplicateEntityException("More than user contest photo #FIXME");

            return results.get(0);
        }
    }

    @Override
    public List<Photo> getAll() {
        try (Session session = sessionFactory.openSession()) {
            var query = session.createQuery("from Photo", Photo.class);
            return query.getResultList();
        }
    }

    @Override
    public List<Photo> getAll(Filter filter) {

        try (Session session = sessionFactory.openSession()) {
            var query = buildConditionalPhotoQuery(filter, session);
            return query.getResultList();
        }
    }

    @Override
    public List<Photo> getAllNotEvaluated(Contest contest, int jurorId) {
        try (Session session = sessionFactory.openSession()) {
            var query = session.createQuery("from Photo ph " +
                            "where ph.contest.contestId = :contestId " +
                            "and ph.id not in (select distinct pe.photo.id from PhotoEvaluation pe " +
                            "where pe.review.juror.id = :jurorId )",
                    Photo.class);

            query.setParameter("contestId", contest.getContestId());
            query.setParameter("jurorId", jurorId);

            return query.getResultList();
        }
    }

    @Transactional
    @Override
    public Photo create(Photo photo) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(photo);
            session.getTransaction().commit();
            return photo;
        }
    }

    @Transactional
    @Override
    public Photo delete(Photo photo) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(photo);
            session.getTransaction().commit();
            return photo;
        }
    }

    private Query<Photo> buildConditionalPhotoQuery(Filter filter, Session session) {

        StringBuilder queryBuilder = new StringBuilder("from Photo ph");
        List<String> additions = new ArrayList<>();

        if (filter.containsOption(CATEGORY_ID))
            additions.add("  ph.contest.category.categoryId = :categoryId ");

        if (filter.containsOption(CONTEST_ID))
            additions.add("  ph.contest.id = :contestId ");

        if (filter.containsOption(AUTHOR_ID))
            additions.add(" ph.author.id = :authorId ");

        if (filter.containsOption(JUROR_ID))
            additions.add(" ph.photoId not in (select phr.photo.photoId from PhotoReview phr where " +
                    "phr.photo.contest.contestId = :contestId and phr.review.juror.userId = :jurorId) ");

        if (filter.containsOption(SEARCH))
            additions.add(" (ph.title like :search or ph.story like :search " +
                    "or ph.contest.title like :search " +
                    "or contest.category.name like :search) ");

        if (!additions.isEmpty())
            queryBuilder.append(" where ")
                    .append(String.join(" and ", additions));

        var query = session.createQuery(queryBuilder.toString(), Photo.class);

        if (filter.containsOption(CATEGORY_ID))
            query.setParameter("categoryId", Integer.parseInt(filter.get(CATEGORY_ID)));

        if (filter.containsOption(CONTEST_ID))
            query.setParameter("contestId", Integer.parseInt(filter.get(CONTEST_ID)));

        if (filter.containsOption(AUTHOR_ID))
            query.setParameter("authorId", Integer.parseInt(filter.get(AUTHOR_ID)));

        if (filter.containsOption(JUROR_ID))
            query.setParameter("jurorId", Integer.parseInt(filter.get(JUROR_ID)));

        if (filter.containsOption(SEARCH))
            query.setParameter("search", QueryHelper.getAsPartialSearchParam(filter.get(SEARCH)));

        return query;
    }
}
