package finalproject.photocontest.repos;

import finalproject.photocontest.config.PhotoStorageConfig;
import finalproject.photocontest.exceptions.PhotoStorageException;
import finalproject.photocontest.repos.contracts.PhotoResourceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

import static finalproject.photocontest.constants.repositories.PhotoRepositoryConstants.*;

@Service
public class PhotoResourceRepositoryFileSystem implements PhotoResourceRepository {

    private final Path rootLocation;

    @Autowired
    public PhotoResourceRepositoryFileSystem(PhotoStorageConfig config) {
        this.rootLocation = Paths.get(config.getLocation());
    }

    @Override
    public Resource loadAsResource(String filename) {
        try {
            Path file = load(filename);
            Resource resource = new UrlResource(file.toUri());

            if (resource.exists() || resource.isReadable())
                return resource;
            else
                throw new PhotoStorageException(COULD_NOT_READ_FILE_EX_MESSAGE + filename);

        } catch (MalformedURLException e) {
            throw new PhotoStorageException(COULD_NOT_READ_FILE_EX_MESSAGE + filename, e);
        }
    }

    @Override
    public void store(MultipartFile file, String persistentName) {

        try {
            Path destinationFile =
                    rootLocation.resolve(persistentName).normalize().toAbsolutePath();

            try (InputStream inputStream = file.getInputStream()) {
                Files.copy(inputStream, destinationFile, StandardCopyOption.REPLACE_EXISTING);
            }

        } catch (IOException e) {
            throw new PhotoStorageException(FAILED_TO_STORE_FILE_EX_MESSAGE, e);
        }
    }

    @Override
    public void deleteByName(String name) {
        var file = load(name);
        try {
            Files.delete(file);
        } catch (IOException e) {
            throw new PhotoStorageException(FAILED_TO_DELETE_EX_MESSAGE + name);
        }
    }

    private Path load(String filename) {
        return rootLocation.resolve(filename);
    }
}

