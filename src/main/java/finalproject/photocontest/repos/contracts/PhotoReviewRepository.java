package finalproject.photocontest.repos.contracts;

import finalproject.photocontest.models.PhotoEvaluation;
import org.springframework.transaction.annotation.Transactional;

public interface PhotoReviewRepository {

    PhotoEvaluation getPhotoReviewByJurorAndPhotoId(int jurorId, int photoId);

    @Transactional
    PhotoEvaluation create(PhotoEvaluation photoEvaluation);
}
