package finalproject.photocontest.repos.contracts;

import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

public interface PhotoResourceRepository {

    Resource loadAsResource(String filename);

    void store(MultipartFile file, String persistentName);

    void deleteByName(String name);
}
