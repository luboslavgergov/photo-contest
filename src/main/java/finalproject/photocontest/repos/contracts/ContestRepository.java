package finalproject.photocontest.repos.contracts;

import finalproject.photocontest.models.Contest;
import finalproject.photocontest.models.ContestParticipant;
import finalproject.photocontest.models.Filter;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface ContestRepository extends ReadOnlyRepository<Contest>{

    Contest getByTitle(String title);

    List<Contest> getAll(Filter filter);

    List<Contest> getAllNotProcessed();

    List<Contest> getAllAvailable(Filter filter);

    @Transactional
    Contest create(Contest contest);

    @Transactional
    Contest enroll(ContestParticipant participant);

    @Transactional
    Contest delete(Contest contest);

    List<Contest> getAllInEvaluation();
}
