package finalproject.photocontest.repos.contracts;

import finalproject.photocontest.models.Contest;
import finalproject.photocontest.models.ContestParticipant;
import finalproject.photocontest.models.Filter;
import finalproject.photocontest.models.User;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface UserRepository extends ReadOnlyRepository<User>, CudRepository<User> {

    User getInactiveById(int id);

    User getByUsername(String username);

    User getActiveByUsername(String username);

    User getActiveByCredentials(String username, String password);

    List<User> getAllActiveJunkies();

    List<User> getAllJunkiesEligibleToInvite(int contestId);

    ContestParticipant getContestParticipant(int contestId, int userId);

    List<ContestParticipant> getAllContestParticipants(int contestId);

    int getContestAwardsCountFor(int userId, int placeFinished);

    List<User> getAll(Filter filter);

    @Transactional
    Contest cancelEnrolment(int contestId, int userId);

    @Transactional
    User restore(User userToRestore);

    List<User> getTopRankedUsers(int count);
}
