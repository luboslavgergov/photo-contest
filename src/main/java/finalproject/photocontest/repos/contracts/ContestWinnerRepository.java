package finalproject.photocontest.repos.contracts;

import finalproject.photocontest.models.ContestWinner;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface ContestWinnerRepository {

    List<ContestWinner> getAll();

    List<ContestWinner> getByPlaceFinished(int contestId, int place);

    @Transactional
    ContestWinner create(ContestWinner winner);

    List<ContestWinner> getTopRankedWinners(int count);
}
