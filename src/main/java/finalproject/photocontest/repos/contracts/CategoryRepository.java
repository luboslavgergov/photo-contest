package finalproject.photocontest.repos.contracts;

import finalproject.photocontest.models.ContestCategory;

public interface CategoryRepository extends ReadOnlyRepository<ContestCategory> {
    ContestCategory getByName(String name);
}
