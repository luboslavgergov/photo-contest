package finalproject.photocontest.repos.contracts;

import finalproject.photocontest.models.*;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface PhotoObjectRepository extends ReadOnlyRepository<Photo> {

    Photo getByUserAndContestId(int userId, int contestId);

    List<Photo> getAll(Filter filter);

    List<Photo> getAllNotEvaluated(Contest contest, int jurorId);

    @Transactional
    Photo create(Photo photo);

    @Transactional
    Photo delete(Photo photo);
}
