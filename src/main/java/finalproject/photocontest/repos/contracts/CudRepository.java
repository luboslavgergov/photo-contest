package finalproject.photocontest.repos.contracts;

import org.springframework.transaction.annotation.Transactional;

public interface CudRepository<T> {

    @Transactional
    T create(T entity);

    @Transactional
    T update(T entity);

    @Transactional
    T delete(T entity);
}
