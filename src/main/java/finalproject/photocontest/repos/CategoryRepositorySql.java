package finalproject.photocontest.repos;

import finalproject.photocontest.exceptions.EntityNotFoundException;
import finalproject.photocontest.models.ContestCategory;
import finalproject.photocontest.repos.contracts.CategoryRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CategoryRepositorySql implements CategoryRepository {
    private final SessionFactory sessionFactory;

    @Autowired
    public CategoryRepositorySql(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public ContestCategory getById(int id) {

        try (Session session = sessionFactory.openSession()) {
            ContestCategory category = session.get(ContestCategory.class, id);
            if (category == null) throw new EntityNotFoundException("Category", id);

            return category;
        }
    }

    @Override
    public ContestCategory getByName(String name) {
        try (Session session = sessionFactory.openSession()) {
            Query<ContestCategory> query = session.createQuery(
                    "from ContestCategory c where c.name = :name", ContestCategory.class);

            query.setParameter("name", name);

            List<ContestCategory> result = query.getResultList();
            if (result.size() == 0) {
                throw new EntityNotFoundException("Category", "name", name);
            }
            return result.get(0);
        }
    }

    @Override
    public List<ContestCategory> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<ContestCategory> query = session.createQuery("from ContestCategory ",
                    ContestCategory.class);
            return query.getResultList();
        }
    }
}
