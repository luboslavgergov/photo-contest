package finalproject.photocontest.exceptions;

public class AuthenticationException extends RuntimeException {

    public AuthenticationException(String username) {
        super(String.format("Authentication failed for user '%s'", username));
    }

    public AuthenticationException() {
        super("Wrong username or password");
    }
}
