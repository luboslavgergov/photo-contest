package finalproject.photocontest.constants.validations;

public class PhotoConstants {
    public static final String PHOTO_ALREADY_EVALUATED_EX_MESSAGE = "Photo already evaluated";
    public static final String PHOTO_CANNOT_STORE_EMPTY_FILES_EX_MESSAGE = "Cannot store empty files";
    public static final String PHOTO_INVALID_CONTENT_TYPE_EX_MESSAGE = "Invalid content type";
    public static final String PHOTO_FILE_EXTENSION_NOT_ALLOWED_EX_MESSAGE = "Photo file extension not allowed";
}
