package finalproject.photocontest.constants.validations;

public class UserConstants {
    public static final String CONTEST_USER_NOT_ELIGIBLE_AS_JUROR_EX_MESSAGE = "User is not eligible as juror";
    public static final String USER_USERNAME_ALREADY_TAKEN_EX_MESSAGE = "Username already taken";
    public static final String USER_IS_NOT_JUNKIE_EX_MESSAGE = "User is not a junkie";
    public static final String USER_IS_NOT_ORGANIZER_EX_MESSAGE = "User is not organizer";
    public static final String USER_NOT_ELIGIBLE_FOR_JUROR_DUTY_EX_MESSAGE = "User not eligible for juror duty";
    public static final String USER_IS_JUROR_FOR_CONTEST_CANNOT_PARTICIPATE_EX_MESSAGE = "User is juror for this contest and cannot participate";
}
