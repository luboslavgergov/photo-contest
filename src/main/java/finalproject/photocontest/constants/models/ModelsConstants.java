package finalproject.photocontest.constants.models;

public class ModelsConstants {
    public static final String USERNAME_VALIDATION_MESSAGE = "Username must be 2-20 symbols long";
    public static final String PASSWORD_VALIDATION_MESSAGE = "Password must be 8-20 symbols long";
    public static final String FIRST_NAME_VALIDATION_MESSAGE = "First name must be 2-20 symbols long";
    public static final String LAST_NAME_VALIDATION_MESSAGE = "Last name must be 2-20 symbols long";

    public static final String PHOTO_TITLE_VALIDATION_MESSAGE = "Last name must be 2-50 symbols long";
    public static final String CATEGORY_ID_VALIDATION_MESSAGE = "Category ID must be positive";
    public static final String PHOTO_STORY_VALIDATION_MESSAGE = "Story field cannot be empty";

    public static final String PHOTO_REVIEW_SCORE_VALIDATION_MESSAGE = "Score must be in the interval 0-10";
    public static final String PHOTO_REVIEW_COMMENT_VALIDATION_MESSAGE = "Comment cannot be empty";
    public static final String PHOTO_REVIEW_COMMENT_LENGTH_VALIDATION_MESSAGE = "Comment must be 5 - 500 symbols long";

    public static final String CONTEST_TITLE_VALIDATION_MESSAGE = "Contest title must be 5-50 symbols long.";
    public static final String CONTEST_START_PHASE_I_VALIDATION_MESSAGE = "Start date of Phase I must be set in future time.";
    public static final String CONTEST_START_PHASE_II_VALIDATION_MESSAGE = "Start date of Phase II must be set anything from one day to one month after start of Phase I";
    public static final String CONTEST_END_VALIDATION_MESSAGE = "The end of the contest must be set anything from one hour to one day after start of Phase II";

    public static final String DEFAULT_CONTEST_COVER = "https://imgur.com/07Wn6vT.png";
    public static final String DEFAULT_USER_AVATAR = "https://imgur.com/f90SaHj.png";

}

