package finalproject.photocontest.config;

import org.springframework.context.annotation.Configuration;

@Configuration
public class PhotoStorageConfig {

    private String location = "src/main/resources/static/images/photo-contest";

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }
}
