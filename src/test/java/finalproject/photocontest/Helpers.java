package finalproject.photocontest;

import finalproject.photocontest.models.*;

import java.util.Set;

public class Helpers {
    public static User createJunkie() {
        var mockUser = new User();
        mockUser.setUserId(1);
        mockUser.setFirstName("Luboslav");
        mockUser.setLastName("Gergov");
        mockUser.setCredentials(createCredentials());
        mockUser.setUserRole(createRoleJunkie());
        return mockUser;
    }

    public static User createOrganizer() {
        var mockUser = new User();
        mockUser.setUserId(2);
        mockUser.setFirstName("Luboslav");
        mockUser.setLastName("Gergov");
        mockUser.setCredentials(createCredentials());
        mockUser.setUserRole(createRoleOrganizer());
        return mockUser;
    }

    public static User createJuror() {
        var mockUser = new User();
        mockUser.setUserId(3);
        mockUser.setFirstName("Luboslav");
        mockUser.setLastName("Gergov");
        mockUser.setCredentials(createCredentials());
        mockUser.setUserRole(createRoleJunkie());
        mockUser.setRankingPoints(800);

        return mockUser;
    }

    public static Credentials createCredentials() {
        var mockUserDetails = new Credentials();
        mockUserDetails.setUserId(1);
        mockUserDetails.setUsername("luboslav");
        mockUserDetails.setPassword("gergov");
        return mockUserDetails;
    }

    public static Credentials createCredentialsV2() {
        var mockUserDetails = new Credentials();
        mockUserDetails.setUserId(1);
        mockUserDetails.setUsername("hristo");
        mockUserDetails.setPassword("minkov");
        return mockUserDetails;
    }

    public static UserRole createRoleJunkie() {
        var mockUserRole = new UserRole();
        mockUserRole.setRoleId(1);
        mockUserRole.setName("junkie");
        return mockUserRole;
    }

    public static UserRole createRoleOrganizer() {
        var mockUserRole = new UserRole();
        mockUserRole.setRoleId(2);
        mockUserRole.setName("organizer");
        return mockUserRole;
    }

    public static ContestCategory createContestCategory() {
        var mockCategory = new ContestCategory();
        mockCategory.setCategoryId(1);
        mockCategory.setName("Interior");
        return mockCategory;
    }

    public static Photo createPhoto() {
        var mockPhoto = new Photo();
        mockPhoto.setPhotoId(1);
        mockPhoto.setTitle("Beautiful");
        mockPhoto.setStory("I took this picture on the pick of mt. Vitosha 2 week ago.");
        mockPhoto.setExtension(".jpg");
        mockPhoto.setAuthor(createJunkie());
        mockPhoto.setContest(createContest());
        mockPhoto.setReviews(Set.of(createReview()));

        return mockPhoto;
    }

    public static Contest createContest() {
        var mockContest = new Contest();
        mockContest.setContestId(1);
        mockContest.setCategory(createContestCategory());
        mockContest.setParticipants(Set.of(createJunkie()));
        mockContest.setJurors(Set.of(createJuror()));

        return mockContest;
    }

    public static Review createReview() {
        var mockReview = new Review();
        mockReview.setReviewId(1);
        mockReview.setComment("This is my favourite photo of this contest.");
        mockReview.setScore(9);
        mockReview.setJuror(createJuror());

        return mockReview;
    }

    public static ContestWinner createContestWinner() {
        var mockContestWinner = new ContestWinner();
        mockContestWinner.setId(1);
        mockContestWinner.setContest(createContest());
        mockContestWinner.setPhoto(createPhoto());
        return mockContestWinner;
    }

    public static PhotoEvaluation createPhotoEvaluation(){
        var mockPhotoEvaluation=new PhotoEvaluation();
        mockPhotoEvaluation.setId(1);
        mockPhotoEvaluation.setPhoto(createPhoto());
        mockPhotoEvaluation.setReview(createReview());

        return mockPhotoEvaluation;
    }
}
