package finalproject.photocontest.utils;

import finalproject.photocontest.exceptions.*;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ExceptionHandlerTests {

    @Test
    public void process_Should_Return_404_When_EntityNotFoundException() {

        // Arrange, Act
        int code = ExceptionHandler.process(new EntityNotFoundException("")).getStatus().value();

        // Assert
        assertEquals(404, code);
    }

    @Test
    public void process_Should_Return_409_When_DuplicateEntityException() {

        // Arrange, Act
        int code = ExceptionHandler.process(new DuplicateEntityException("")).getStatus().value();

        // Assert
        assertEquals(409, code);
    }

    @Test
    public void process_Should_Return_401_When_AuthorizationException() {

        // Arrange, Act
        int code = ExceptionHandler.process(new AuthorizationException("")).getStatus().value();

        // Assert
        assertEquals(401, code);
    }


    @Test
    public void process_Should_Return_403_When_AuthenticationException() {

        // Arrange, Act
        int code = ExceptionHandler.process(new AuthenticationException("")).getStatus().value();

        // Assert
        assertEquals(403, code);
    }

    @Test
    public void process_Should_Return_412_When_IllegalRequestException() {

        // Arrange, Act
        int code = ExceptionHandler.process(new IllegalRequestException("")).getStatus().value();

        // Assert
        assertEquals(412, code);
    }

    @Test
    public void process_Should_Return_204_When_IPhotoStorageException() {

        // Arrange, Act
        int code = ExceptionHandler.process(new PhotoStorageException("")).getStatus().value();

        // Assert
        assertEquals(204, code);
    }

    @Test
    public void process_Should_Return_204_When_UnsupportedPhotoFileExtensionException(){

        // Arrange, Act
        int code = ExceptionHandler.process(new UnsupportedPhotoFileExtensionException("")).getStatus().value();

        // Assert
        assertEquals(507, code);
    }

    @Test
    public void process_Should_Return_500_When_ExceptionNotExpected() {

        // Arrange, Act
        int code = ExceptionHandler.process(new NullPointerException()).getStatus().value();

        // Assert
        assertEquals(500, code);
    }
}
