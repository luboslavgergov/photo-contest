package finalproject.photocontest.services;

import finalproject.photocontest.Helpers;
import finalproject.photocontest.exceptions.EntityNotFoundException;
import finalproject.photocontest.exceptions.IllegalRequestException;
import finalproject.photocontest.exceptions.PhotoStorageException;
import finalproject.photocontest.models.Filter;
import finalproject.photocontest.models.Photo;
import finalproject.photocontest.models.User;
import finalproject.photocontest.repos.contracts.PhotoObjectRepository;
import finalproject.photocontest.repos.contracts.PhotoResourceRepository;
import finalproject.photocontest.utils.contracts.PhotoResourceHandler;
import finalproject.photocontest.validation.ContestValidationServiceImpl;
import finalproject.photocontest.validation.PhotoValidationServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

@ExtendWith(MockitoExtension.class)
public class PhotoServicesTests {

    @Mock
    PhotoResourceRepository mockPhotoFileRepository;

    @Mock
    PhotoObjectRepository mockPhotoObjectRepository;

    @Mock
    PhotoResourceHandler mockPhotoResourceHandler;

    @Mock
    ContestValidationServiceImpl mockContestValidationService;

    @Mock
    PhotoValidationServiceImpl mockPhotoValidationService;

    @Mock
    MockMultipartFile mockMultipartFile;

    @InjectMocks
    PhotoServiceImpl photoService;

    private static Filter filter;
    private static Photo photo;
    private static MultipartFile file;
    private static User junkie;


    @BeforeEach
    private void setup() {
        filter = new Filter();
        photo = Helpers.createPhoto();
        junkie = Helpers.createJunkie();
    }


    @Test
    public void getAll_Should_CallRepository() {
        //Arrange
        Mockito.when(mockPhotoObjectRepository.getAll())
                .thenReturn(null);

        //Act
        photoService.getAll();

        //Assert
        Mockito.verify(mockPhotoObjectRepository, Mockito.times(1)).getAll();
    }

    @Test
    public void getAll_Should_CallRepository_When_FilterIsUsed() {

        //Arrange
        Mockito.when(mockPhotoObjectRepository.getAll(filter))
                .thenReturn(null);

        //Act
        photoService.getAll(filter);

        //Assert
        Mockito.verify(mockPhotoObjectRepository, Mockito.times(1)).getAll(filter);
    }

    @Test
    public void getById_Should_CallRepository() {

        //Arrange
        Mockito.when(mockPhotoObjectRepository.getById(Mockito.anyInt())).thenReturn(null);

        //Act
        photoService.getById(Mockito.anyInt());

        //Assert
        Mockito.verify(mockPhotoObjectRepository, Mockito.times(1))
                .getById(Mockito.anyInt());
    }

    @Test
    public void getById_Should_Throw_When_PhotoWithIdNotExist() {

        // Arrange
        Mockito.when(mockPhotoObjectRepository.getById(Mockito.anyInt())).thenThrow(new EntityNotFoundException());

        //Act, Assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> photoService.getById(Mockito.anyInt()));
    }

    @Test
    public void createPhoto_Should_CallRepository_When_PhotoDataConsistent() {
        //Arrange
        Mockito.when(mockContestValidationService.ensureEnrolled(photo.getContest(),photo.getAuthor()))
                .thenReturn(mockContestValidationService);
        Mockito.when(mockContestValidationService.ensureUserHasNoPhotoUploaded(Mockito.any(), Mockito.any()))
                .thenReturn(mockContestValidationService);
        Mockito.when(mockContestValidationService.ensureInEnrollmentPhase(photo.getContest()))
                .thenReturn(mockContestValidationService);

        Mockito.when(mockPhotoValidationService.ensureFileNotEmpty(mockMultipartFile))
                .thenReturn(mockPhotoValidationService);
        Mockito.when(mockPhotoValidationService.ensureFileContentType(mockMultipartFile))
                .thenReturn(mockPhotoValidationService);
        Mockito.when(mockPhotoValidationService.ensureFileExtension(mockMultipartFile))
                .thenReturn(mockPhotoValidationService);

        Mockito.when(mockMultipartFile.getContentType()).thenReturn("image/jpg");

        // Act
        photoService.create(photo, mockMultipartFile);

        //Assert
        Mockito.verify(mockPhotoResourceHandler, Mockito.times(1))
                .persistMultipartPhoto(photo, mockMultipartFile);
    }

    @Test
    public void createPhoto_Should_Throw_When_UserNotEnrolledForTheContest() {
        //Arrange
        Mockito.when(mockContestValidationService.ensureEnrolled(photo.getContest(),photo.getAuthor()))
                .thenThrow(new IllegalRequestException());

        //Act, Assert
        Assertions.assertThrows(IllegalRequestException.class,
                () -> photoService.create(photo, file));

    }

    @Test
    public void createPhoto_Should_Throw_When_UserAlreadyUploadedPhoto() {
        //Arrange
        Mockito.when(mockContestValidationService.ensureEnrolled(photo.getContest(),photo.getAuthor()))
                .thenReturn(mockContestValidationService);
        Mockito.when(mockContestValidationService.ensureUserHasNoPhotoUploaded(Mockito.any(), Mockito.any()))
                .thenThrow(new IllegalRequestException());

        //Act, Assert
        Assertions.assertThrows(IllegalRequestException.class,
                () -> photoService.create(photo, file));

    }

    @Test
    public void createPhoto_Should_Throw_When_ContestPastEnrollmentPhase() {
        //Arrange
        Mockito.when(mockContestValidationService.ensureEnrolled(photo.getContest(),photo.getAuthor()))
                .thenReturn(mockContestValidationService);
        Mockito.when(mockContestValidationService.ensureUserHasNoPhotoUploaded(Mockito.any(), Mockito.any()))
                .thenReturn(mockContestValidationService);
        Mockito.when(mockContestValidationService.ensureInEnrollmentPhase(photo.getContest()))
                .thenThrow(new IllegalRequestException());

        //Act, Assert
        Assertions.assertThrows(IllegalRequestException.class,
                () -> photoService.create(photo, file));
    }

    @Test
    public void createPhoto_Should_Throw_When_FileIsEmpty() {
        //Arrange
        Mockito.when(mockContestValidationService.ensureEnrolled(photo.getContest(),photo.getAuthor()))
                .thenReturn(mockContestValidationService);
        Mockito.when(mockContestValidationService.ensureUserHasNoPhotoUploaded(Mockito.any(), Mockito.any()))
                .thenReturn(mockContestValidationService);
        Mockito.when(mockContestValidationService.ensureInEnrollmentPhase(photo.getContest()))
                .thenReturn(mockContestValidationService);

        Mockito.when(mockPhotoValidationService.ensureFileNotEmpty(file))
                .thenThrow(new PhotoStorageException());
        //Act, Assert
        Assertions.assertThrows(PhotoStorageException.class,
                () -> photoService.create(photo, file));
    }

    @Test
    public void createPhoto_Should_Throw_When_InvalidFileExtension() {
        //Arrange
        Mockito.when(mockContestValidationService.ensureEnrolled(photo.getContest(),photo.getAuthor()))
                .thenReturn(mockContestValidationService);
        Mockito.when(mockContestValidationService.ensureUserHasNoPhotoUploaded(Mockito.any(), Mockito.any()))
                .thenReturn(mockContestValidationService);
        Mockito.when(mockContestValidationService.ensureInEnrollmentPhase(photo.getContest()))
                .thenReturn(mockContestValidationService);

        Mockito.when(mockPhotoValidationService.ensureFileNotEmpty(file))
                .thenReturn(mockPhotoValidationService);
        Mockito.when(mockPhotoValidationService.ensureFileContentType(file))
                .thenReturn(mockPhotoValidationService);
        Mockito.when(mockPhotoValidationService.ensureFileExtension(file))
                .thenThrow(new PhotoStorageException());

        //Act, Assert
        Assertions.assertThrows(PhotoStorageException.class,
                () -> photoService.create(photo, file));
    }

    @Test
        public void createPhoto_Should_Throw_When_InvalidContentType() {
        //Arrange
        Mockito.when(mockContestValidationService.ensureEnrolled(photo.getContest(),photo.getAuthor()))
                .thenReturn(mockContestValidationService);
        Mockito.when(mockContestValidationService.ensureUserHasNoPhotoUploaded(Mockito.any(), Mockito.any()))
                .thenReturn(mockContestValidationService);
        Mockito.when(mockContestValidationService.ensureInEnrollmentPhase(photo.getContest()))
                .thenReturn(mockContestValidationService);

        Mockito.when(mockPhotoValidationService.ensureFileNotEmpty(file))
                .thenReturn(mockPhotoValidationService);
        Mockito.when(mockPhotoValidationService.ensureFileContentType(file))
                .thenThrow(new PhotoStorageException());

        //Act, Assert
        Assertions.assertThrows(PhotoStorageException.class,
                () -> photoService.create(photo, file));
    }

    @Test
    public void loadContestPhotoFileAsResource_Should_CallRepository_When_RequestValid() {
        // Arrange
        Mockito.when(mockContestValidationService.ensurePhotoPresent(1, 1))
                .thenReturn(mockContestValidationService);

        Mockito.when(mockPhotoObjectRepository.getById(1)).thenReturn(photo);

        // Act
        photoService.loadContestPhotoFileAsResource(1, 1,junkie);

        // Assert
        Mockito.verify(mockPhotoFileRepository, Mockito.times(1))
                .loadAsResource("1.1.jpg");
    }

    @Test
    public void loadContestPhotoFileAsResource_Should_Throw_When_PhotoNotInContest() {
        //Arrange
        Mockito.when(mockContestValidationService.ensurePhotoPresent(Mockito.anyInt(), Mockito.anyInt()))
                .thenThrow(new IllegalRequestException());

        //Act, Assert
        Assertions.assertThrows(IllegalRequestException.class,
                () -> photoService.loadContestPhotoFileAsResource(Mockito.anyInt(), Mockito.anyInt(),junkie));
    }

    @Test
    public void loadContestPhotoFileAsResource_Should_Throw_When_PhotoDoesNotExistInContest() {
        // Arrange
        Mockito.when(mockContestValidationService.ensurePhotoPresent(1, 1))
                .thenReturn(mockContestValidationService);

        Mockito.when(mockPhotoObjectRepository.getById(1))
                .thenThrow(new EntityNotFoundException());

        //Act, Assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> photoService.loadContestPhotoFileAsResource(1, 1,junkie));
    }

    @Test
    public void getContestPhotoObjectById_Should_CallRepository_When_RequestValid() {
        //Arrange
        Mockito.when(mockContestValidationService.ensurePhotoPresent(1, 1))
                .thenReturn(mockContestValidationService);

        //Act
        photoService.getByContestIdAndPhotoId(1, 1,junkie);

        //Assert
        Mockito.verify(mockPhotoObjectRepository, Mockito.times(1))
                .getById(1);
    }

    @Test
    public void getContestPhotoObjectById_Should_Throw_When_PhotoDoesNotExistInContest() {
        //Arrange
        Mockito.when(mockContestValidationService.ensurePhotoPresent(1, 1))
                .thenThrow(new IllegalRequestException());

        //Act, Assert
        Assertions.assertThrows(IllegalRequestException.class,
                () -> photoService.getByContestIdAndPhotoId(1, 1,junkie));
    }

}
