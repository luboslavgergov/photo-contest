package finalproject.photocontest.services;

import finalproject.photocontest.Helpers;
import finalproject.photocontest.exceptions.EntityNotFoundException;
import finalproject.photocontest.models.ContestCategory;
import finalproject.photocontest.repos.contracts.CategoryRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class CategoryServiceTests {

    @Mock
    CategoryRepository mockRepository;

    @InjectMocks
    CategoryServiceImpl categoryService;


    @Test
    public void getAll_Should_ReturnAllCategories_When_CountriesExist() {
        //Arrange
        Mockito.when(mockRepository.getAll())
                .thenReturn(null);

        //Act
        categoryService.getAll();

        //Assert
        Mockito.verify(mockRepository, Mockito.times(1)).getAll();
    }


    @Test
    public void getById_Should_ReturnCategory_When_CategoryExists() {
        //Arrange
        Mockito.when(mockRepository.getById(1))
                .thenReturn(null);

        //Act
        categoryService.getById(1);

        //Assert
        Mockito.verify(mockRepository, Mockito.times(1)).getById(1);
    }

    @Test
    public void getById_Should_Throw_When_CountryDoesNotExists() {
        //Arrange
        Mockito.when(mockRepository.getById(100))
                .thenThrow(new EntityNotFoundException());

        //Act, Assert
        Assertions.assertThrows(EntityNotFoundException.class, () -> categoryService.getById(100));
    }

    @Test
    public void getByName_Should_ReturnCategory_When_MatchExists() {
        //Arrange
        ContestCategory mockCategory = Helpers.createContestCategory();
        Mockito.when(mockRepository.getByName("category"))
                .thenReturn(null);

        //Act
        categoryService.getByName("category");

        //Assert
        Mockito.verify(mockRepository, Mockito.times(1)).getByName("category");
    }

    @Test
    public void getByName_Should_Throw_When_CategoryDoesNotExists() {
        //Arrange
        Mockito.when(mockRepository.getByName("Toys"))
                .thenThrow(new EntityNotFoundException());

        //Act, Assert
        Assertions.assertThrows(EntityNotFoundException.class, () -> categoryService.getByName("Toys"));
    }


}

