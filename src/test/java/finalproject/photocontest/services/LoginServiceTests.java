package finalproject.photocontest.services;

import finalproject.photocontest.Helpers;
import finalproject.photocontest.exceptions.AuthenticationException;
import finalproject.photocontest.exceptions.AuthorizationException;
import finalproject.photocontest.exceptions.EntityNotFoundException;
import finalproject.photocontest.models.User;
import finalproject.photocontest.services.contracts.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpHeaders;
import org.springframework.util.LinkedMultiValueMap;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(MockitoExtension.class)
public class LoginServiceTests {

    @Mock
    UserService mockUserService;

    @InjectMocks
    LoginServiceImpl loginService;

    private HttpHeaders headers;
    private User user;
    private User organizer;

    @BeforeEach
    public void setup() {
        var map = new LinkedMultiValueMap<String, String>();
        map.add("Username", "user");
        map.add("Password", "pass");
        headers = new HttpHeaders(map);
        user = Helpers.createJunkie();
        organizer=Helpers.createOrganizer();
    }

    @Test
    public void authenticateUser_Should_NotThrow_When_CredentialsValid() {

        // Arrange
        Mockito.when(mockUserService.getByCredentials(Mockito.anyString(), Mockito.anyString()))
                .thenReturn(user);

        // Act
        loginService.authenticateUser(headers);

        // Assert
        Mockito.verify(mockUserService, Mockito.times(1))
                .getByCredentials(Mockito.anyString(), Mockito.anyString());
    }

    @Test
    public void authenticateUser_Should_Throw_When_CredentialsNotValid() {

        // Arrange
        Mockito.when(mockUserService.getByCredentials(Mockito.anyString(), Mockito.anyString()))
                .thenThrow(EntityNotFoundException.class);

        // Act, Assert
        assertThrows(AuthenticationException.class, () -> loginService.authenticateUser(headers));
    }


    @Test
    public void authorizeUser_Should_ReturnUser_When_junkieRoleMatching() {

        // Arrange, Act
        var result = loginService.authorizeRole(user, "junkie");

        // Assert
        assertEquals(result, user);
    }

    @Test
    public void authorizeUser_Should_ReturnUser_When_RoleMatching() {

        // Arrange, Act
        var result = loginService.authorizeRole(organizer,  "organizer");

        // Assert
        assertEquals(result, organizer);
    }

    @Test
    public void authorizeUser_Should_Throw_When_NotAuthorized() {


        //Arrange, Act, Assert
        assertThrows(AuthorizationException.class,
                () -> loginService.authorizeRole(user, "organizer"));
    }

    @Test
    public void authorizeUser_Should_Throw_When_NoMatchingRoles() {
        loginService.authorizeRole(user, "junkie", "organizer");

        //Arrange, Act, Assert
        assertThrows(AuthorizationException.class,
                () -> loginService.authorizeRole(user, "adminnn"));
    }
}
