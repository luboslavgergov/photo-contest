package finalproject.photocontest.services;

import finalproject.photocontest.Helpers;
import finalproject.photocontest.exceptions.EntityNotFoundException;
import finalproject.photocontest.exceptions.IllegalRequestException;
import finalproject.photocontest.exceptions.PhotoStorageException;
import finalproject.photocontest.models.Contest;
import finalproject.photocontest.models.Filter;
import finalproject.photocontest.models.User;
import finalproject.photocontest.models.enums.ContestPhase;
import finalproject.photocontest.models.enums.FilterOption;
import finalproject.photocontest.repos.contracts.ContestRepository;
import finalproject.photocontest.repos.contracts.UserRepository;
import finalproject.photocontest.utils.contracts.PhotoResourceHandler;
import finalproject.photocontest.validation.ContestValidationServiceImpl;
import finalproject.photocontest.validation.UserValidationServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDateTime;

@ExtendWith(MockitoExtension.class)
public class ContestServiceImplTests {

    @Mock
    ContestRepository mockContestRepository;

    @Mock
    ContestValidationServiceImpl mockContestValidationService;

    @Mock
    UserValidationServiceImpl mockUserValidationService;

    @Mock
    UserRepository mockUserRepository;

    @Mock
    PhotoResourceHandler mockPhotoResourceHandler;

    @InjectMocks
    ContestServiceImpl contestService;

    private static Filter filter;
    private static Contest contest;
    private static User junkie;
    private static User juror;


    @BeforeEach
    private void setup() {
        filter = new Filter();
        contest = Helpers.createContest();
        junkie = Helpers.createJunkie();
        juror = Helpers.createJuror();
    }

    @Test
    public void getAll_Should_CallRepository() {
        //Arrange
        Mockito.when(mockContestRepository.getAll())
                .thenReturn(null);

        //Act
        contestService.getAll();

        //Assert
        Mockito.verify(mockContestRepository, Mockito.times(1)).getAll();
    }

    @Test
    public void getAll_Should_CallRepository_When_FilterIsUsed() {
        //Arrange
        Mockito.when(mockContestRepository.getAll(filter))
                .thenReturn(null);

        //Act
        contestService.getAll(filter);

        //Assert
        Mockito.verify(mockContestRepository, Mockito.times(1)).getAll(filter);
    }

    @Test
    public void getContestsAvailableToUser_Should_CallRepository_When_FilterIsUsed() {
        //Arrange
        Mockito.when(mockContestRepository.getAllAvailable(filter))
                .thenReturn(null);

        //Act
        contestService.getContestsAvailableToUser(filter);

        //Assert
        Mockito.verify(mockContestRepository, Mockito.times(1)).getAllAvailable(filter);
    }

    @Test
    public void getById_Should_CallRepository() {
        //Arrange
        Mockito.when(mockContestRepository.getById(1)).thenReturn(null);

        //Act
        contestService.getById(1);

        //Assert
        Mockito.verify(mockContestRepository, Mockito.times(1))
                .getById(1);
    }

    @Test
    public void getById_Should_Throw_When_ContestDoesNotExist() {
        //Arrange
        Mockito.when(mockContestRepository.getById(Mockito.anyInt()))
                .thenThrow(new EntityNotFoundException());

        //Act, Assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> contestService.getById(Mockito.anyInt()));
    }

    @Test
    public void createContest_Should_CallRepository_When_ContestDataConsistent() {
        //Arrange
        Mockito.when(mockContestValidationService.ensureTitleUnique(contest))
                .thenReturn(mockContestValidationService);
        Mockito.when(mockContestValidationService.ensureScheduleTimelineConsistent(contest))
                .thenReturn(mockContestValidationService);
        Mockito.when(mockContestValidationService.ensurePhaseDurationsConsistent(contest))
                .thenReturn(mockContestValidationService);
        Mockito.when(mockContestValidationService.ensureJurorsEligible(contest))
                .thenReturn(mockContestValidationService);

        //Act
        contestService.create(contest);

        //Assert
        Mockito.verify(mockContestRepository, Mockito.times(1)).create(contest);
    }

    @Test
    public void createContest_Should_Throw_When_ContestNameAlreadyExist() {
        //Arrange
        Mockito.when(mockContestValidationService.ensureTitleUnique(Mockito.any()))
                .thenThrow(new IllegalRequestException());

        //Act, Assert
        Assertions.assertThrows(IllegalRequestException.class,
                () -> contestService.create(Mockito.any()));
    }

    @Test
    public void createContest_Should_Throw_When_ContestPhasesNotScheduledCorrect() {
        //Arrange
        Mockito.when(mockContestValidationService.ensureTitleUnique(Mockito.any()))
                .thenReturn(mockContestValidationService);
        Mockito.when(mockContestValidationService.ensureScheduleTimelineConsistent(Mockito.any()))
                .thenThrow(new IllegalRequestException());

        //Act, Assert
        Assertions.assertThrows(IllegalRequestException.class,
                () -> contestService.create(Mockito.any()));
    }

    @Test
    public void createContest_Should_Throw_When_When_DurationOfPhaseIsIncorrect() {
        //Arrange
        Mockito.when(mockContestValidationService.ensureTitleUnique(Mockito.any()))
                .thenReturn(mockContestValidationService);
        Mockito.when(mockContestValidationService.ensureScheduleTimelineConsistent(Mockito.any()))
                .thenReturn(mockContestValidationService);
        Mockito.when(mockContestValidationService.ensurePhaseDurationsConsistent(Mockito.any()))
                .thenThrow(new IllegalRequestException());

        //Act, Assert
        Assertions.assertThrows(IllegalRequestException.class,
                () -> contestService.create(Mockito.any()));
    }

    @Test
    public void createContest_Should_Throw_When_When_IllegalJuror() {
        //Arrange
        Mockito.when(mockContestValidationService.ensureTitleUnique(Mockito.any()))
                .thenReturn(mockContestValidationService);
        Mockito.when(mockContestValidationService.ensureScheduleTimelineConsistent(Mockito.any()))
                .thenReturn(mockContestValidationService);
        Mockito.when(mockContestValidationService.ensurePhaseDurationsConsistent(Mockito.any()))
                .thenReturn(mockContestValidationService);
        Mockito.when(mockContestValidationService.ensureJurorsEligible(Mockito.any()))
                .thenThrow(new IllegalRequestException());

        //Act, Assert
        Assertions.assertThrows(IllegalRequestException.class,
                () -> contestService.create(Mockito.any()));
    }

    @Test
    public void getTimeTillNextPhase_Should_ShowRemainingTime_When_ContestPhaseNotStarted() {
        //Arrange
        contest.setStartPhase1(LocalDateTime.now().plusDays(5));
        Mockito.when(mockContestRepository.getById(1)).thenReturn(contest);

        //Act
        contestService.getTimeTillNextPhase(1);

        //Assert
        Mockito.verify(mockContestRepository, Mockito.times(1)).getById(1);
        Assertions.assertEquals(contest.getPhase(), ContestPhase.NOT_STARTED);
    }

    @Test
    public void getTimeTillNextPhase_Should_ShowRemainingTime_When_ContestIsInPhaseI() {
        //Arrange
        contest.setStartPhase1(LocalDateTime.now().minusDays(10));
        contest.setStartPhase2(LocalDateTime.now().plusDays(5));
        Mockito.when(mockContestRepository.getById(1)).thenReturn(contest);

        //Act
        contestService.getTimeTillNextPhase(1);

        //Assert
        Mockito.verify(mockContestRepository, Mockito.times(1)).getById(1);
        Assertions.assertEquals(contest.getPhase(), ContestPhase.PHASE_I);


    }

    @Test
    public void getTimeTillNextPhase_Should_ShowRemainingTime_When_ContestIsInPhaseII() {
        //Arrange
        contest.setStartPhase1(LocalDateTime.now().minusDays(31));
        contest.setStartPhase2(LocalDateTime.now().minusDays(1));
        contest.setEndPhase2(LocalDateTime.now().plusDays(1));
        Mockito.when(mockContestRepository.getById(1)).thenReturn(contest);

        //Act
        contestService.getTimeTillNextPhase(1);

        //Assert
        Mockito.verify(mockContestRepository, Mockito.times(1)).getById(1);
        Assertions.assertEquals(contest.getPhase(), ContestPhase.PHASE_II);
    }

    @Test
    public void getTimeTillNextPhase_Should_Throw_When_ContestDoesNotExist() {
        //Arrange
        Mockito.when(mockContestRepository.getById(Mockito.anyInt())).
                thenThrow(new EntityNotFoundException());

        //Act, Assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> contestService.getTimeTillNextPhase(Mockito.anyInt()));
    }

    @Test
    public void enroll_Should_addJunkieToContest_When_ContestIsValid() {
        //Arrange
        Mockito.when(mockUserRepository.getById(Mockito.anyInt()))
                .thenReturn(junkie);
        Mockito.when(mockContestRepository.getById(Mockito.anyInt()))
                .thenReturn(contest);

        Mockito.when(mockUserValidationService.ensureIsJunkie(junkie))
                .thenReturn(mockUserValidationService);
        Mockito.when(mockUserValidationService.ensureIsNotJuror(junkie, contest))
                .thenReturn(mockUserValidationService);

        Mockito.when(mockContestValidationService.ensureInEnrollmentPhase(contest))
                .thenReturn(mockContestValidationService);
        Mockito.when(mockContestValidationService.ensureOpen(contest))
                .thenReturn(mockContestValidationService);
        Mockito.when(mockContestValidationService.ensureNotAlreadyEnrolled(contest, junkie))
                .thenReturn(mockContestValidationService);

        //Act
        contestService.enroll(1, 1);

        //Assert
        Mockito.verify(mockUserRepository, Mockito.times(1)).getById(1);
    }

    @Test
    public void cancelEnrollment_Should_removeJunkieFromContest_When_ContestIsNotStarted() {
        //Arrange
        Mockito.when(mockContestRepository.getById(Mockito.anyInt()))
                .thenReturn(contest);
        Mockito.when(mockUserRepository.getById(Mockito.anyInt()))
                .thenReturn(junkie);

        Mockito.when(mockContestValidationService.ensureInEnrollmentPhase(contest))
                .thenReturn(mockContestValidationService);
        Mockito.when(mockContestValidationService.ensureEnrolled(contest, junkie))
                .thenReturn(mockContestValidationService);

        //Act
        contestService.cancelEnrollment(1, 1);

        //Assert
        Mockito.verify(mockUserRepository, Mockito.times(1))
                .cancelEnrolment(1, 1);
    }

    @Test
    public void cancelEnrollment_Should_Throw_When_ContestDoesNotExist() {
        //Arrange
        Mockito.when(mockContestRepository.getById(1))
                .thenThrow(new EntityNotFoundException());

        //Act, Assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> contestService.cancelEnrollment(1, 1));

    }

    @Test
    public void cancelEnrollment_Should_Throw_When_UserDoesNotExist() {
        //Arrange
        Mockito.when(mockContestRepository.getById(Mockito.anyInt()))
                .thenReturn(contest);
        Mockito.when(mockUserRepository.getById(Mockito.anyInt()))
                .thenThrow(new EntityNotFoundException());


        //Act, Assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> contestService.cancelEnrollment(1, 1));
    }

    @Test
    public void cancelEnrollment_Should_Throw_When_ContestPastEnrollmentPhase() {
        //Arrange
        Mockito.when(mockContestRepository.getById(Mockito.anyInt()))
                .thenReturn(contest);
        Mockito.when(mockUserRepository.getById(Mockito.anyInt()))
                .thenReturn(junkie);

        Mockito.when(mockContestValidationService.ensureInEnrollmentPhase(contest))
                .thenThrow(new IllegalRequestException());

        //Act, Assert
        Assertions.assertThrows(IllegalRequestException.class,
                () -> contestService.cancelEnrollment(1, 1));
    }

    @Test
    public void cancelEnrollment_Should_Throw_When_UserAlreadyParticipate() {
        //Arrange
        Mockito.when(mockContestRepository.getById(Mockito.anyInt()))
                .thenReturn(contest);
        Mockito.when(mockUserRepository.getById(Mockito.anyInt()))
                .thenReturn(junkie);

        Mockito.when(mockContestValidationService.ensureInEnrollmentPhase(contest))
                .thenReturn(mockContestValidationService);
        Mockito.when(mockContestValidationService.ensureEnrolled(contest, junkie))
                .thenThrow(new IllegalRequestException());

        //Act, Assert
        Assertions.assertThrows(IllegalRequestException.class,
                () -> contestService.cancelEnrollment(1, 1));
    }

    @Test
    public void forceRemoveJunkieWithoutPhoto_Should_RemoveJunkie_When_PhotoIsNotUploaded() {
        //Arrange
        Mockito.when(mockUserRepository.cancelEnrolment(Mockito.anyInt(), Mockito.anyInt()))
                .thenReturn(null);

        //Act
        contestService.forceRemoveJunkieWithoutPhoto(Mockito.anyInt(), Mockito.anyInt());

        //Assert
        Mockito.verify(mockUserRepository, Mockito.times(1))
                .cancelEnrolment(Mockito.anyInt(), Mockito.anyInt());
    }

    @Test
    public void forceRemoveJunkieWithoutPhoto_Should_Throw_When_FailedToDeleteByName() {
        //Arrange
        Mockito.when(mockUserRepository.cancelEnrolment(Mockito.anyInt(), Mockito.anyInt()))
                .thenThrow(new PhotoStorageException());

        //Act, Assert
        Assertions.assertThrows(PhotoStorageException.class,
                () -> contestService.forceRemoveJunkieWithoutPhoto(Mockito.anyInt(), Mockito.anyInt()));

    }

    @Test
    public void removePhoto_Should_RemovePhoto_When_ValidParameters() {
        //Arrange
        Mockito.when(mockContestRepository.getById(1))
                .thenReturn(contest);
        Mockito.when(mockUserRepository.getById(1))
                .thenReturn(junkie);

        Mockito.when(mockContestValidationService.ensureUserEnrolled(contest, junkie))
                .thenReturn(mockContestValidationService);
        Mockito.when(mockContestValidationService.ensureInEnrollmentPhase(contest))
                .thenReturn(mockContestValidationService);
        Mockito.when(mockContestValidationService.ensureUserUploadedPhoto(junkie, contest))
                .thenReturn(mockContestValidationService);

        Mockito.when(mockPhotoResourceHandler.deleteMultipartPhotoIfPresent(1, 1))
                .thenReturn(true);

        //Act
        contestService.removePhoto(1, 1);

        //Assert
        Mockito.verify(mockUserRepository, Mockito.times(1))
                .getById(1);
        Mockito.verify(mockUserRepository, Mockito.times(1))
                .getById(1);
    }


    @Test
    public void removePhoto_Should_Throw_When_ContestDoesNotExist() {
        //Arrange
        Mockito.when(mockContestRepository.getById(1))
                .thenThrow(new EntityNotFoundException());

        //Act, Assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> contestService.removePhoto(1, 1));

    }

    @Test
    public void removePhoto_Should_Throw_When_UserDoesNotExist() {
        //Arrange
        Mockito.when(mockContestRepository.getById(Mockito.anyInt()))
                .thenReturn(contest);
        Mockito.when(mockUserRepository.getById(Mockito.anyInt()))
                .thenThrow(new EntityNotFoundException());


        //Act, Assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> contestService.removePhoto(1, 1));
    }

    @Test
    public void removePhoto_Should_Throw_When_UserNotEnrolledForThisContest() {
        //Arrange
        Mockito.when(mockContestRepository.getById(Mockito.anyInt()))
                .thenReturn(contest);
        Mockito.when(mockUserRepository.getById(Mockito.anyInt()))
                .thenReturn(junkie);

        Mockito.when(mockContestValidationService.ensureUserEnrolled(contest, junkie))
                .thenThrow(new IllegalRequestException());

        //Act, Assert
        Assertions.assertThrows(IllegalRequestException.class,
                () -> contestService.removePhoto(1, 1));
    }

    @Test
    public void removePhoto_Should_Throw_When_ContestEnrollmentIsOver() {
        //Arrange
        Mockito.when(mockContestRepository.getById(Mockito.anyInt()))
                .thenReturn(contest);
        Mockito.when(mockUserRepository.getById(Mockito.anyInt()))
                .thenReturn(junkie);

        Mockito.when(mockContestValidationService.ensureUserEnrolled(contest, junkie))
                .thenReturn(mockContestValidationService);
        Mockito.when(mockContestValidationService.ensureInEnrollmentPhase(contest))
                .thenThrow(new IllegalRequestException());

        //Act, Assert
        Assertions.assertThrows(IllegalRequestException.class,
                () -> contestService.removePhoto(1, 1));
    }

    @Test
    public void removePhoto_Should_Throw_When_DeleteFileFailed() {
        //Arrange
        Mockito.when(mockContestRepository.getById(1))
                .thenReturn(contest);
        Mockito.when(mockUserRepository.getById(1))
                .thenReturn(junkie);

        Mockito.when(mockContestValidationService.ensureUserEnrolled(contest, junkie))
                .thenReturn(mockContestValidationService);
        Mockito.when(mockContestValidationService.ensureInEnrollmentPhase(contest))
                .thenReturn(mockContestValidationService);
        Mockito.when(mockContestValidationService.ensureUserUploadedPhoto(junkie, contest))
                .thenReturn(mockContestValidationService);

        Mockito.when(mockPhotoResourceHandler.deleteMultipartPhotoIfPresent(1, 1))
                .thenReturn(false);

        //Act, Assert
        Assertions.assertThrows(IllegalStateException.class,
                () -> contestService.removePhoto(1, 1));
    }

    @Test
    public void delete_Should_CallRepository_When_RequestValid() {
        //Arrange
        Mockito.when(mockContestRepository.getById(Mockito.anyInt()))
                .thenReturn(contest);
        Mockito.when(mockContestValidationService.ensureNotVisibleToUsers(contest))
                .thenReturn(mockContestValidationService);

        //Act
        contestService.delete(Mockito.anyInt());

        //Assert
        Mockito.verify(mockContestRepository, Mockito.times(1))
                .delete(contest);
    }

    @Test
    public void delete_Should_Throw_When_ContestNotExists() {
        //Arrange
        Mockito.when(mockContestRepository.getById(Mockito.anyInt()))
                .thenThrow(new EntityNotFoundException());

        //Act, Assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> contestService.delete(Mockito.anyInt()));
    }

    @Test
    public void delete_Should_Throw_When_UserNotEligibleForJuror() {
        //Arrange
        Mockito.when(mockContestRepository.getById(Mockito.anyInt()))
                .thenReturn(Mockito.any());
        Mockito.when(mockContestValidationService.ensureNotVisibleToUsers(contest))
                .thenThrow(new IllegalRequestException());

        //Act, Assert
        Assertions.assertThrows(IllegalRequestException.class,
                () -> contestService.delete(Mockito.anyInt()));
    }


    @Test
    public void getAllNotProcessed_Should_CallRepository() {
        //Arrange
        Mockito.when(mockContestRepository.getAllNotProcessed())
                .thenReturn(null);

        //Act
        contestService.getAllNotProcessed();

        //Assert
        Mockito.verify(mockContestRepository, Mockito.times(1))
                .getAllNotProcessed();
    }

    @Test
    public void getContestsAsJuror_Should_CallRepository() {
        //Arrange
        filter.add(FilterOption.JUROR_ID, "1");
        Mockito.when(mockUserRepository.getById(1))
                .thenReturn(juror);
        Mockito.when(mockUserValidationService.ensureIsEligibleForJuror(Mockito.any()))
                .thenReturn(mockUserValidationService);

        //Act
        contestService.getContestsAsJuror(filter);

        //Assert
        Mockito.verify(mockUserRepository, Mockito.times(1))
                .getById(Mockito.anyInt());
    }

    @Test
    public void getContestsAsJuror_Should_Throw_When_UserDoesNotExists() {
        //Arrange
        filter.add(FilterOption.JUROR_ID, "1");
        Mockito.when(mockUserRepository.getById(1))
                .thenThrow(new EntityNotFoundException());

        //Act, Assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> contestService.getContestsAsJuror(filter));

    }

    @Test
    public void getContestsAsJuror_Should_Throw_When_IsNotEligibleForJuror() {
        //Arrange
        filter.add(FilterOption.JUROR_ID, "1");
        Mockito.when(mockUserRepository.getById(1))
                .thenReturn(juror);
        Mockito.when(mockUserValidationService.ensureIsEligibleForJuror(Mockito.any()))
                .thenThrow(new IllegalRequestException());

        //Act, Assert
        Assertions.assertThrows(IllegalRequestException.class,
                () -> contestService.getContestsAsJuror(filter));

    }

    @Test
    public void getAllInEvaluationPhase_Should_CallRepository() {
        //Arrange
        Mockito.when(mockContestRepository.getAllInEvaluation())
                .thenReturn(null);

        //Act
        contestService.getAllInEvaluationPhase();

        //Assert
        Mockito.verify(mockContestRepository, Mockito.times(1))
                .getAllInEvaluation();
    }

    @Test
    public void inviteUserToContest_Should_AddJunkieToPrivateContest_When_ContestIsValid() {
        //Arrange
        Mockito.when(mockUserRepository.getById(Mockito.anyInt()))
                .thenReturn(junkie);
        Mockito.when(mockContestRepository.getById(Mockito.anyInt()))
                .thenReturn(contest);

        Mockito.when(mockUserValidationService.ensureIsJunkie(junkie))
                .thenReturn(mockUserValidationService);
        Mockito.when(mockUserValidationService.ensureIsNotJuror(junkie, contest))
                .thenReturn(mockUserValidationService);

        Mockito.when(mockContestValidationService.ensureInEnrollmentPhase(contest))
                .thenReturn(mockContestValidationService);
        Mockito.when(mockContestValidationService.ensureNotAlreadyEnrolled(contest, junkie))
                .thenReturn(mockContestValidationService);

        //Act
        contestService.inviteUserToContest(1, 1);

        //Assert
        Mockito.verify(mockUserRepository, Mockito.times(1))
                .getById(1);
        Mockito.verify(mockContestRepository, Mockito.times(1))
                .getById(1);
    }

    @Test
    public void inviteUserToContest_Should_Throw_When_UserDoesNotExits() {
        //Arrange
        Mockito.when(mockUserRepository.getById(Mockito.anyInt()))
                .thenThrow(new EntityNotFoundException());

        //Act, Assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> contestService.inviteUserToContest(1, 1));
    }

    @Test
    public void inviteUserToContest_Should_Throw_When_ContestDoesNotExists() {
        //Arrange
        Mockito.when(mockUserRepository.getById(Mockito.anyInt()))
                .thenReturn(junkie);
        Mockito.when(mockContestRepository.getById(Mockito.anyInt()))
                .thenThrow(new EntityNotFoundException());

        //Act, Assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> contestService.inviteUserToContest(1, 1));
        Mockito.verify(mockUserRepository, Mockito.times(1)).getById(1);
    }

    @Test
    public void inviteUserToContest_Should_Throw_When_UserIsNotJunkie() {
        //Arrange
        Mockito.when(mockUserRepository.getById(Mockito.anyInt()))
                .thenReturn(junkie);
        Mockito.when(mockContestRepository.getById(Mockito.anyInt()))
                .thenReturn(contest);

        Mockito.when(mockUserValidationService.ensureIsJunkie(junkie))
                .thenThrow(new IllegalRequestException());

        //Act, Assert
        Assertions.assertThrows(IllegalRequestException.class,
                () -> contestService.inviteUserToContest(1, 1));
        Mockito.verify(mockUserRepository, Mockito.times(1))
                .getById(1);
        Mockito.verify(mockContestRepository, Mockito.times(1))
                .getById(1);
    }

    @Test
    public void inviteUserToContest_Should_Throw_When_UserIsAJuror() {
        //Arrange
        Mockito.when(mockUserRepository.getById(Mockito.anyInt()))
                .thenReturn(junkie);
        Mockito.when(mockContestRepository.getById(Mockito.anyInt()))
                .thenReturn(contest);

        Mockito.when(mockUserValidationService.ensureIsJunkie(junkie))
                .thenReturn(mockUserValidationService);
        Mockito.when(mockUserValidationService.ensureIsNotJuror(junkie, contest))
                .thenThrow(new IllegalRequestException());

        //Act, Assert
        Assertions.assertThrows(IllegalRequestException.class,
                () -> contestService.inviteUserToContest(1, 1));
        Mockito.verify(mockUserRepository, Mockito.times(1))
                .getById(1);
        Mockito.verify(mockContestRepository, Mockito.times(1))
                .getById(1);
    }

    @Test
    public void inviteUserToContest_Should_Throw_When_UserAlreadyEnrolled() {
        //Arrange
        Mockito.when(mockUserRepository.getById(Mockito.anyInt()))
                .thenReturn(junkie);
        Mockito.when(mockContestRepository.getById(Mockito.anyInt()))
                .thenReturn(contest);

        Mockito.when(mockUserValidationService.ensureIsJunkie(junkie))
                .thenReturn(mockUserValidationService);
        Mockito.when(mockUserValidationService.ensureIsNotJuror(junkie, contest))
                .thenReturn(mockUserValidationService);

        Mockito.when(mockContestValidationService.ensureInEnrollmentPhase(contest))
                .thenThrow(new IllegalRequestException());

        //Act, Assert
        Assertions.assertThrows(IllegalRequestException.class,
                () -> contestService.inviteUserToContest(1, 1));
        Mockito.verify(mockUserRepository, Mockito.times(1))
                .getById(1);
        Mockito.verify(mockContestRepository, Mockito.times(1))
                .getById(1);
    }

    @Test
    public void inviteUserToContest_Should_Throw_When_ContestPhaseNotEnrollment() {
        //Arrange
        Mockito.when(mockUserRepository.getById(Mockito.anyInt()))
                .thenReturn(junkie);
        Mockito.when(mockContestRepository.getById(Mockito.anyInt()))
                .thenReturn(contest);

        Mockito.when(mockUserValidationService.ensureIsJunkie(junkie))
                .thenReturn(mockUserValidationService);
        Mockito.when(mockUserValidationService.ensureIsNotJuror(junkie, contest))
                .thenReturn(mockUserValidationService);

        Mockito.when(mockContestValidationService.ensureInEnrollmentPhase(contest))
                .thenReturn(mockContestValidationService);
        Mockito.when(mockContestValidationService.ensureNotAlreadyEnrolled(contest, junkie))
                .thenThrow(new IllegalRequestException());

        //Act, Assert
        Assertions.assertThrows(IllegalRequestException.class,
                () -> contestService.inviteUserToContest(1, 1));
        Mockito.verify(mockUserRepository, Mockito.times(1))
                .getById(1);
        Mockito.verify(mockContestRepository, Mockito.times(1))
                .getById(1);
    }


}
